<?php
namespace Sdk\Dictionary\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullDictionaryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullDictionary::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsDictionary()
    {
        $this->assertInstanceof('Sdk\Dictionary\Model\Dictionary', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
