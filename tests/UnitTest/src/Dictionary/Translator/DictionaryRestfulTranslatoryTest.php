<?php
namespace Sdk\Dictionary\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Model\NullDictionary;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Translator\CrewRestfulTranslator;

class DictionaryRestfulTranslatorTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = new DictionaryRestfulTranslator();

        $this->childStub = new class extends DictionaryRestfulTranslator {
            public function getCrewRestfulTranslator()
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Dictionary());
        $this->assertInstanceOf('Sdk\Dictionary\Model\NullDictionary', $result);
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function setMethods(Dictionary $dictionary, array $attributes, array $relationships)
    {
        if (isset($attributes['name'])) {
            $dictionary->setName($attributes['name']);
        }
        if (isset($attributes['remark'])) {
            $dictionary->setRemark($attributes['remark']);
        }
        if (isset($attributes['parentId'])) {
            $dictionary->setParentId($attributes['parentId']);
        }
        if (isset($attributes['category'])) {
            $dictionary->setCategory($attributes['category']);
        }
        if (isset($attributes['status'])) {
            $dictionary->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $dictionary->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $dictionary->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $dictionary->setStatusTime($attributes['statusTime']);
        }

        if (isset($relationships['crew']['data'])) {
            $dictionary->setCrew(new Crew($relationships['crew']['data']['id']));
        }

        return $dictionary;
    }

    public function testArrayToObjectCorrectObject()
    {
        $this->stub = $this->getMockBuilder(DictionaryRestfulTranslator::class)
            ->setMethods(['getCrewRestfulTranslator'])
            ->getMock();

        $dictionary = \Sdk\Dictionary\Utils\MockFactory::generateDictionaryArray();

        $datas =  $dictionary['data'];
        $relationships = $datas['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);

        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($dictionary);

        $expectObject = new Dictionary();

        $expectObject->setId($datas['id']);

        $attributes = isset($datas['attributes']) ? $datas['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $this->stub = $this->getMockBuilder(DictionaryRestfulTranslator::class)
            ->setMethods(['getCrewRestfulTranslator'])
            ->getMock();

        $dictionary = \Sdk\Dictionary\Utils\MockFactory::generateDictionaryArray();

        $data =  $dictionary['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);

        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($dictionary);

        $expectArray = array();

        $expectObject = new Dictionary();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $dictionary = \Sdk\Dictionary\Utils\MockFactory::generateDictionaryObject(1, 1);

        $actual = $this->stub->objectToArray($dictionary);

        $expectedArray = array(
            'data'=>array(
                'type'=>'dictionaries',
            )
        );

        $expectedArray['data']['attributes'] = array(
            'name'=>$dictionary->getName(),
            'remark'=>$dictionary->getRemark(),
            'parentId'=>$dictionary->getParentId(),
            'category'=>$dictionary->getCategory(),
            'status'=>$dictionary->getStatus(),
        );

        $expectedArray['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $dictionary->getCrew()->getId()
            )
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
