<?php
namespace Sdk\Dictionary\Repository;

use Sdk\Dictionary\Adapter\Dictionary\IDictionaryAdapter;
use Sdk\Dictionary\Adapter\Dictionary\DictionaryRestfulAdapter;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Sdk\Dictionary\Utils\MockFactory;

class DictionaryRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = new DictionaryRepository();

        $this->childStub = new class extends DictionaryRepository {
            public function getAdapter() : DictionaryRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getActualAdapter() : IDictionaryAdapter
            {
                return parent::getActualAdapter();
            }
            public function getMockAdapter() : IDictionaryAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Adapter\Dictionary\DictionaryRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Adapter\Dictionary\IDictionaryAdapter',
            $this->childStub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Adapter\Dictionary\IDictionaryAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为DictionaryRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以DictionaryRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $this->stub = $this->getMockBuilder(DictionaryRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $adapter = $this->prophesize(DictionaryRestfulAdapter::class);
        $adapter->scenario(Argument::exact(DictionaryRepository::LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(DictionaryRepository::LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
