<?php
namespace Sdk\Dictionary\Adapter\Dictionary;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Utils\MockFactory;
use Sdk\Dictionary\Model\NullDictionary;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

class DictionaryRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(DictionaryRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends DictionaryRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIDictionaryAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Adapter\Dictionary\IDictionaryAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('dictionaries', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Translator\DictionaryRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'DICTIONARY_LIST',
                DictionaryRestfulAdapter::SCENARIOS['DICTIONARY_LIST']
            ],
            [
                'DICTIONARY_FETCH_ONE',
                DictionaryRestfulAdapter::SCENARIOS['DICTIONARY_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $dictionary = MockFactory::generateDictionaryObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullDictionary())
            ->willReturn($dictionary);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($dictionary, $result);
    }
    /**
     * 为DictionaryRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$dictionary，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareDictionaryTranslator(
        Dictionary $dictionary,
        array $keys,
        array $dictionaryArray
    ) {
        $translator = $this->prophesize(DictionaryRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($dictionary),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($dictionaryArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Dictionary $dictionary)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($dictionary);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDictionaryTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $dictionary = MockFactory::generateDictionaryObject(1);
        $dictionaryArray = array();

        $this->prepareDictionaryTranslator(
            $dictionary,
            array(
                'name',
                'remark',
                'category',
                'parentId',
                'crew'
            ),
            $dictionaryArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('dictionaries', $dictionaryArray);

        $this->success($dictionary);

        $result = $this->stub->add($dictionary);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDictionaryTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $dictionary = MockFactory::generateDictionaryObject(1);
        $dictionaryArray = array();

        $this->prepareDictionaryTranslator(
            $dictionary,
            array(
                'name',
                'remark',
                'category',
                'parentId',
                'crew'
            ),
            $dictionaryArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('dictionaries', $dictionaryArray);

        $this->failure($dictionary);
        $result = $this->stub->add($dictionary);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDictionaryTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $dictionary = MockFactory::generateDictionaryObject(1);
        $dictionaryArray = array();

        $this->prepareDictionaryTranslator(
            $dictionary,
            array(
                'name',
                'remark',
                'parentId'
            ),
            $dictionaryArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'dictionaries/'.$dictionary->getId(),
                $dictionaryArray
            );

        $this->success($dictionary);

        $result = $this->stub->edit($dictionary);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDictionaryTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $dictionary = MockFactory::generateDictionaryObject(1);
        $dictionaryArray = array();

        $this->prepareDictionaryTranslator(
            $dictionary,
            array(
                'name',
                'remark',
                'parentId'
            ),
            $dictionaryArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'dictionaries/'.$dictionary->getId(),
                $dictionaryArray
            );

        $this->failure($dictionary);
        $result = $this->stub->edit($dictionary);
        $this->assertFalse($result);
    }
}
