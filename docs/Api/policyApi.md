# 政策API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [重新编辑](#重新编辑)
	* [上架](#上架)
	* [下架](#下架)

---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称              | 类型        |请求参数是否必填  |  示例                                                          | 描述            |
| :---:                | :----:     | :------:       |:------------:                                                 |:-------:       |
| title                | string     | 是             | 宜昌高新区（自贸片区）政务服务局 关于调整建设项目中介服务机构备选库的通知  | 标题            |
| number               | string     |  自动生成       | ZC201903021                                                    | 政策编号         |
| applicableObjects    | array      | 是，可多选      | 1 微型企业，2 小型企业，3 中型企业，4 大型企业，5 创业个人，6 个体商户，7 其他| 适用对象    |
| dispatchDepartments   | array      | 是，可多选。     |  国务院                                        | 发文部门     |
| applicableIndustries | array      | 是，可多选       | 1 矿产品（煤、磷）精细化工，2 生物医学，3 食品饮料，4 纺织服装，5 化学纤维，6 家具制品，7 造纸和印刷品，8 橡胶和塑料，9 通用设备，10 专用设备，11 汽车制造， 12 船舶、航空航天，13 电气机械，14 通信电子 仪器仪表，15 电力、热力供应，16 其他        | 适用行业         |
| level                 | int       | 是              | 1 国家级，2 省级，3 市级，4 地区，5 其他                                           | 政策级别|
| classifies             | array      | 是，可多选        | 1 产业政策，2 金融支持，3 环境发展，4 财政扶持，5 创业创新，6 税收优惠，7 综合政策 	| 政策分类|
| detail               | array     	| 是             | 文本内容，图片地址                                                  | 政策详情     |
| description          | string     | 是             |    政策描述                                                        | 政策描述     |
| image			       | array		| 否			  |  array('name'=>'封面图', 'identify'=>'封面.JPG')                   | 政策封面图   |
| attachments 	       | array		| 否			   |  array(array('name'=>'附件', 'identify'=>'附件.zip'))              | 政策附件    |
| labels     	       | array		| 是	    	 |  省时                                                             | 标签        |
| admissibleAddress    | array		| 否			 |  array(array('address'=>'西安市雁塔区', 'longitude'=>'91.2', 'latitude'=>'34.5')) | 受理地址|
| processingFlow       | array     	| 否              | 文本内容，图片地址          | 办理流程 |
| status        	   | int        |                 | 0       | 状态  (0上架 -2下架)|
| pageviews        	   | int        |                 | 0       | 浏览量    |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(PolicyRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(PolicyRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(PolicyRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
    "data"=>array(
    	"type"=>"policies",
    	"attributes"=>array(
    	    "title"=>"政策标题",
    	    "applicableObjects"=>array(1,2),
    	    "applicableIndustries"=>array(1,2),
    	    "level"=>1,
    	    "classifies"=>array(1,2),
    	    "detail"=>array(array("type"=>"text", "value"=>"文本内容")),
    	    "image"=>array("name"=>"封面图","identify"=>封面图.jpg”),
    	    "attachments"=>array(array("name"=>"政策附件","identify"=>"政策附件.zip")),
    	    "description"=>"政策描述",
    	    "processingFlow"=>array(array("type"=>"text", "value"=>"办理流程")),
    	    "admissibleAddress"=>array(
    			array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
    			array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4")
    		),
    	),
    	"relationships"=>array(
    		"crew"=>array(
    			"data"=>array(
    				array("type"=>"crews", "id"=>1)
    			)
    		),
    		"dispatchDepartments"=>array(
    			"data"=>array(
    				array("type"=>"dispatchDepartments","id"=>4),
    				array("type"=>"dispatchDepartments","id"=>7),
    			)
    		),
    		"labels"=>array(
    			"data"=>array(
    				array("type"=>"labels","id"=>4),
    				array("type"=>"labels","id"=>7),
    			)
    		)
    	)
    )
);
```
**调用示例**

```
$crew = $this->getCrewRepository()->fetchOne(int $id);

$policy = new Sdk\Policy\Model\Policy;
$policy->setStatus('-2');
$policy->setTitle('政策标题');
$policy->addApplicableObjects(array(1,2));
$policy->addDispatchDepartments($dispatchDepartmentRepository->fetchList(array(1, 2, 3, 4)));
$policy->addApplicableIndustries(array(1,2));
$policy->setLevel(2);
$policy->addClassifies(array(1,2));
$policy->setDetail(array(array("type"=>"text", "value"=>"sss")));
$policy->setDescription('政策描述');
$policy->setImage(array("name"=>"封面图","identify"=>'封面图.jpg'));
$policy->setAttachments(array(array("name"=>"政策附件","identify"=>"政策附件.zip")));
$policy->addLabels($labelsRepository->fetchList(array(1, 2, 3)));
$policy->setAdmissibleAddress(array(
	array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
	array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
	array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4")
));
$policy->setProcessingFlow(array(array("type"=>"text", "value"=>"办理流程")));
$policy->setCrew($crew);

$policy->add();

```
### <a name="重新编辑">重新编辑</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"policies",
        "attributes"=>array(
            "title"=>"政策标题",
            "applicableObjects"=>array(1,2),
            "dispatchDepartment"=>array(1,2),
            "applicableIndustries"=>array(1,2),
            "level"=>1,
            "classifies"=>array(1,2),
            "detail"=>array(array("type"=>"text", "value"=>"文本内容")),
            "image"=>array("name"=>"封面图","identify"=>封面图.jpg”),
            "attachments"=>array(array("name"=>"政策附件","identify"=>"政策附件.zip")),
            "description"=>"政策描述",
            "processingFlow"=>array(array("type"=>"text", "value"=>"办理流程")),
            "admissibleAddress"=>array(
                array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
                array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4")
            ),
        )
        "relationships"=>array(
            "dispatchDepartments"=>array(
                "data"=>array(
                    array("type"=>"dispatchDepartments","id"=>4),
                    array("type"=>"dispatchDepartments","id"=>7),
                )
            ),
            "labels"=>array(
                "data"=>array(
                    array("type"=>"labels","id"=>4),
                    array("type"=>"labels","id"=>7),
                )
            )
        )
    )
);
```
**调用示例**

```
$policy = $this->getRepository()->fetchOne(int $id);
$dispatchDepartmentRepository = new Sdk\DispatchDepartment\Repository\DispatchDepartmentRepository();
$labelsRepository = new Sdk\Label\Repository\LabelRepository();

$crew = $this->getCrewRepository()->fetchOne(int $id);

$policy->setStatus('-2');
$policy->setTitle('政策标题');
$policy->addApplicableObjects(array(1,2));
$policy->addDispatchDepartments($dispatchDepartmentRepository->fetchList(array(1, 2, 3, 4)));
$policy->addApplicableIndustries(array(1,2));
$policy->setLevel(2);
$policy->addClassifies(array(1,2));
$policy->setDetail(array(array("type"=>"text", "value"=>"sss")));
$policy->setDescription('政策描述');
$policy->setImage(array("name"=>"封面图","identify"=>'封面图.jpg'));
$policy->setAttachments(array(array("name"=>"政策附件","identify"=>"政策附件.zip")));
$policy->addLabels($labelsRepository->fetchList(array(1, 2, 3)));
$policy->setAdmissibleAddress(array(
	array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
	array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4"),
	array("address"=>"地址","longitude"=>"12.4","latitude"=>"12.4")
));
$policy->setProcessingFlow(array(array("type"=>"text", "value"=>"办理流程")));

$policy->add();

```
### <a name="上架">上架</a>

**调用示例**

```
$policy = $this->getRepository()->fetchOne(int $id);

$policy->onShelf();

```
### <a name="下架">下架</a>

**调用示例**

```
$policy = $this->getRepository()->fetchOne(int $id);

$policy->offStock();

```