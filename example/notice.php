<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Member\Model\Member;

// $businessNotice = new Sdk\BusinessNotice\Model\BusinessNotice;

// $businessNotice->setId(1);
// $businessNotice->setMember(new Member(1));

// var_dump($businessNotice->allRead()); //done

$memberSystemNotice = new Sdk\MemberSystemNotice\Model\MemberSystemNotice;

$memberSystemNotice->setId(1);
$memberSystemNotice->setMember(new Member(1));

var_dump($memberSystemNotice->allRead()); //done

exit();
