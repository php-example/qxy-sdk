<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Crew\Model\Crew;
use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Repository\DictionaryRepository;

$crew = new Crew(1);
$dictionary = new Dictionary();
$dictionaryRepository = new DictionaryRepository();

$dictionary->setId(1);
$dictionary->setStatus(2);
$dictionary->setName('一级分类11111111');
$dictionary->setRemark('备注11111');
$dictionary->setParentId(0);
$dictionary->setCategory(1);
$dictionary->setCrew($crew);

 //var_dump($dictionary->add());
 //var_dump($dictionary->edit());
 //var_dump($dictionary->enable());
//var_dump($dictionary->disable());

// var_dump($dictionaryRepository->fetchOne(1));
// var_dump($dictionaryRepository->fetchList(array(1, 2, 3)));
 exit();
