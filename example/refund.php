<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Member\Model\Member;
use Sdk\Crew\Model\Crew;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\Order\RefundOrder\Model\RefundOrder;

$refundOrder = new RefundOrder();

// $refundOrderRepository = new Sdk\Appointment\Repository\AppointmentRepository();
// $refundOrder = $refundOrderRepository->scenario($refundOrderRepository::FETCH_ONE_MODEL_UN)->fetchOne(2);

$member = new Member(1);
$crew = new Crew(1);
$enterprise = new Enterprise(1);
$serviceOrder = new ServiceOrder();
$serviceOrder->setId(63);

$refundOrder = new RefundOrder();

$refundOrder->setId(2);
$refundOrder->setRefundReason("辣鸡");
$refundOrder->setOrder($serviceOrder);
$refundOrder->setMember($member);
$refundOrder->setEnterprise($enterprise);
$refundOrder->setRefuseReason("你才辣鸡");
$refundOrder->setVoucher(['name'=>'voucher', 'identify' => 'voucher.jpg']);
$refundOrder->setCrew($crew);

// var_dump($refundOrder->add());
// var_dump($refundOrder->approve());
// var_dump($refundOrder->reject());
var_dump($refundOrder->payment());

//  var_dump($refundOrderRepository->scenario($refundOrderRepository::FETCH_ONE_MODEL_UN)->fetchOne(3)); exit();//done
//  var_dump($refundOrderRepository->fetchList(array(1, 2, 3))); exit();//done
// exit();
