<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\LoanRequirement\Model\LoanRequirement;
use Sdk\LoanRequirement\Model\TenderInformation;
use Sdk\LoanRequirement\Model\TransactionInfo;
use Sdk\LoanRequirement\Model\LoanResultInfo;

use Sdk\Member\Model\Member;
use Sdk\Dictionary\Model\Dictionary;
use Sdk\Label\Model\Label;
use Sdk\LoanRequirement\Repository\LoanRequirementRepository;
use Sdk\LoanRequirement\Repository\TenderInformationRepository;
use Sdk\Enterprise\Model\Enterprise;

$loanRequirementRepository = new LoanRequirementRepository();
$loanRequirement = $loanRequirementRepository->scenario(LoanRequirementRepository::FETCH_ONE_MODEL_UN)->fetchOne(13);

// $loanRequirement = new LoanRequirement();

// $member = new Member(1);
// $loanRequirement->setMember($member);

// $loanRequirement->setTitle('测试控件测试重新编辑');
// $loanRequirement->setDescription('发布需求描述');
// $loanRequirement->setLoanObject(1);  // 贷款对象 1 企业 2 个人
// $loanRequirement->setContactName('张三');
// $loanRequirement->setContactPhone('13720406329');
// $loanRequirement->setArea('陕西,西安');
// $loanRequirement->setValidityTime('1560909890');
// $loanRequirement->setLoanPurposeDescribe('贷款用途描述');
// $loanRequirement->setCollateralDescribe('抵押物描述');
// $loanRequirement->setLoanAmount(50);  // 贷款金额
// $loanRequirement->setLoanTerm(20);  // 贷款期限
// $loanRequirement->setIsHaveCollateral(0);
// $loanRequirement->setIsAllowViewCreditInformation(0);
// $loanRequirement->setAttachments(array(
//  array('name'=>'附件名称', 'identify'=>'附件地址.jpg'),
//  array('name'=>'附件名称1', 'identify'=>'附件地址.pdf'),
// ));
// $loanRequirement->setCollateral(new Dictionary(458));
// $loanRequirement->addLabel(new Label(1));
// $loanRequirement->addLabel(new Label(3));
// $loanRequirement->setLoanPurpose(new Dictionary(459));

// var_dump($loanRequirement->add());die;

//var_dump($loanRequirement->approve());die;

// $loanRequirement->setRejectReason('数据不正规');
// var_dump($loanRequirement->reject());die;

//var_dump($loanRequirement->resubmit());die;

//var_dump($loanRequirement->revoke());exit();

// var_dump($loanRequirement->deletes());exit();

// $loanRequirement->setCloseReason('已经不需要了');
// var_dump($loanRequirement->close());exit();

// $loanRequirement->clearTenderInformations();

// $enterprise = new Enterprise(1);
// $tenderInformation = new TenderInformation();

// $tenderInformation->setCreditMinLine(300);
// $tenderInformation->setCreditMaxLine(600);
// $tenderInformation->setCreditMinLoanTerm(28);
// $tenderInformation->setCreditMaxLoanTerm(54);
// $tenderInformation->setCreditValidityTime('1560909891');
// $tenderInformation->setEnterprise($enterprise);

// $loanRequirement->addTenderInformation($tenderInformation);

// var_dump($loanRequirement->bid());exit();

$tenderInformationRepository = new TenderInformationRepository();
$tenderInformation = $tenderInformationRepository->scenario(TenderInformationRepository::FETCH_ONE_MODEL_UN)->fetchOne(12);

// var_dump($loanRequirement->matching($tenderInformation));exit();

// $transactionInfo = new TransactionInfo();
// $transactionInfo->setTransactionTime("法定工作日 12:00-17:00");
// $transactionInfo->setTransactionArea("陕西省,西安市,雁塔区");
// $transactionInfo->setTransactionAddress("长延堡街道");
// $transactionInfo->setContactName("张三");
// $transactionInfo->setContactPhone("029-871233004");
// $transactionInfo->setRemark("注意事项");
// $transactionInfo->setCarryData('携带资料');
// $loanRequirement->setTransactionInfo($transactionInfo);
// var_dump($loanRequirement->transaction($tenderInformation));exit();

$loanResultInfo = new LoanResultInfo();
$loanResultInfo->setLoanFailReason("");
$loanResultInfo->setLoanAmount(100);
$loanResultInfo->setLoanTerm(10);
$loanResultInfo->setRepaymentMethod(1);
$loanResultInfo->setStatus(2);

$loanRequirement->setLoanResultInfo($loanResultInfo);
var_dump($loanRequirement->completed($tenderInformation));
