<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Crew\Model\Crew;
use Sdk\GovernmentOrgans\Model\GovernmentOrgans;
use Sdk\GovernmentOrgans\Repository\GovernmentOrgansRepository;

$crew = new Crew(1);
$governmentOrgans = new GovernmentOrgans();
$governmentOrgansRepository = new GovernmentOrgansRepository();

$governmentOrgans->setName('碑林区政府');
$governmentOrgans->setArea('陕西省,西安市,雁塔区');
$governmentOrgans->setAddress('长延堡街道');
$governmentOrgans->setHotline('029-81543109');
$governmentOrgans->setServiceTime('周一至周五 8:30-15:30');
$governmentOrgans->setCrew($crew);

// var_dump($governmentOrgans->add());


// $governmentOrgans = $governmentOrgansRepository->scenario($governmentOrgansRepository::FETCH_ONE_MODEL_UN)->fetchOne(2);
// var_dump($governmentOrgans->edit());
// var_dump($governmentOrgans->offStock());
// var_dump($governmentOrgans->onShelf());

// var_dump($governmentOrgansRepository->fetchOne(1));
// var_dump($governmentOrgansRepository->fetchList(array(1, 2, 3)));
exit();
