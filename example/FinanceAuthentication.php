<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\FinanceAuthentication\Model\FinanceAuthenticationCategoryFactory;

// financeAuthentication 测试
$financeAuthentication = new Sdk\FinanceAuthentication\Model\FinanceAuthentication();
// unAuditedFinanceAuthentication 测试
$unAuditedFinanceAuthentication = new Sdk\FinanceAuthentication\Model\UnAuditedFinanceAuthentication();


$unAuditedFinanceAuthentication->setId(6);
// $unAuditedFinanceAuthentication->setRejectReason('12312312');
// $unAuditedFinanceAuthentication->setEnterpriseName('哈哈哈哈哈哈xi');
// $unAuditedFinanceAuthentication->setOrganizationsCategory(FinanceAuthenticationCategoryFactory::create(1, FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']));
// $unAuditedFinanceAuthentication->setOrganizationsCategoryParent(FinanceAuthenticationCategoryFactory::create(1, FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']));
// $unAuditedFinanceAuthentication->setCode('123123123123HKK');
// $unAuditedFinanceAuthentication->setLicence(array(
//                 'name' => '金融许可证', 'identify' => '金融许可证.jpg'
//             ));
// $unAuditedFinanceAuthentication->setOrganizationCode('123123123123HKH');
// $unAuditedFinanceAuthentication->setOrganizationCodeCertificate(array(
//                  'name' => '金融机构代码证', 'identify' => '金融机构代码证.jpg'
//              ));
// var_dump($unAuditedFinanceAuthentication->resubmit());die;
var_dump($unAuditedFinanceAuthentication->approve());
die;
// var_dump($unAuditedFinanceAuthentication->reject());die;

// $financeAuthenticationRepository = new Sdk\FinanceAuthentication\Repository\FinanceAuthenticationRepository();
// var_dump($financeAuthenticationRepository->scenario($financeAuthenticationRepository::FETCH_ONE_MODEL_UN)->fetchOne(1));die;

$financeAuthentication->setId(1);
// $financeAuthentication->setEnterpriseName('嘻嘻嘻嘻');
$financeAuthentication->setOrganizationsCategory(FinanceAuthenticationCategoryFactory::create(1, FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']));
$financeAuthentication->setOrganizationsCategoryParent(FinanceAuthenticationCategoryFactory::create(1, FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']));
$financeAuthentication->setCode('123123123123HKH');
$financeAuthentication->setLicence(array(
                'name' => '金融许可证', 'identify' => '金融许可证.jpg'
            ));
$financeAuthentication->setOrganizationCode('9234560912h');
$financeAuthentication->setOrganizationCodeCertificate(array(
                'name' => '金融机构代码证', 'identify' => '金融机构代码证.jpg'
            ));
// $financeAuthentication->setEnterprise(new Sdk\Enterprise\Model\Enterprise(1));

// var_dump($financeAuthentication->add());die;
// var_dump($financeAuthentication->edit());die;
// var_dump($financeAuthentication->resubmit());die;
