<?php
namespace Sdk\Statistical\Model;

class Statistical
{
    const FILTER_TYPE = array(
        'SERVICE'=>1,
        'LOAN_PRODUCT'=>2,
        'POLICY'=>3,
        'POLICY_INTERPRETATION'=>4,
   );

    private $id;

    private $result;

    public function __construct()
    {
        $this->result = array();
    }

    public function __destruct()
    {
        unset($this->result);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setResult(array $result) : void
    {
        $this->result = $result;
    }

    public function getResult() : array
    {
        return $this->result;
    }
}
