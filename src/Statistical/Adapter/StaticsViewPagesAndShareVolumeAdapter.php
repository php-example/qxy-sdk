<?php
namespace Sdk\Statistical\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Statistical\Model\NullStatistical;

class StaticsViewPagesAndShareVolumeAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    use StatisticalAdapterTrait;

    public function analyse(array $filter = array())
    {
        $this->get(
            $this->getResource().'/staticsViewPagesAndShareVolume',
            array(
                'filter'=>$filter['filter'],
                'sort'=>$filter['sort'],
                'page'=>array('size'=>$filter['size'], 'number'=>$filter['page'])
            )
        );
        
        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
