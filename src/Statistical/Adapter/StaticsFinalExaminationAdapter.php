<?php
namespace Sdk\Statistical\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Sdk\Statistical\Model\NullStatistical;

class StaticsFinalExaminationAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    use StatisticalAdapterTrait;

    public function analyse(array $filter = array())
    {
        $this->get(
            $this->getResource().'/staticsFinalExamination',
            array(
                'filter'=>$filter
            )
        );

        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
