<?php
namespace Sdk\Statistical\Adapter;

use Sdk\Statistical\Model\NullStatistical;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

class StaticsTrialBalanceCountAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    use StatisticalAdapterTrait;

    public function analyse(array $filter = array())
    {
        $this->get(
            $this->getResource().'/staticsTrialBalance',
            array(
                'filter'=>$filter
            )
        );

        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
