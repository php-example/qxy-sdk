<?php
namespace Sdk\Statistical\Adapter;

class StatisticalAdapterFactory
{
    const MAPS = array(
        'staticsActiveEnterprise'=>
            'Sdk\Statistical\Adapter\StaticsActiveEnterpriseAdapter',
        'staticsServiceAuthenticationCount'=>
            'Sdk\Statistical\Adapter\StaticsServiceAuthenticationCountAdapter',
        'staticsServiceRequirementCount'=>
            'Sdk\Statistical\Adapter\StaticsServiceRequirementCountAdapter',
        'staticsServiceCount'=>
            'Sdk\Statistical\Adapter\StaticsServiceCountAdapter',
        'staticsMemberCouponCount'=>
            'Sdk\Statistical\Adapter\StaticsMemberCouponCountAdapter',
        'staticsEnterpriseOrderVolume'=>
            'Sdk\Statistical\Adapter\StaticsEnterpriseOrderVolumeAdapter',
        'staticsLoanProductCount'=>
            'Sdk\Statistical\Adapter\StaticsLoanProductCountAdapter',
        'staticsFinanceCount'=>
            'Sdk\Statistical\Adapter\StaticsFinanceCountAdapter',
        'trialBalance'=>
            'Sdk\Statistical\Adapter\TrialBalanceCountAdapter',
        'staticsEnterpriseAuthenticationCount'=>
            'Sdk\Statistical\Adapter\StaticsEnterpriseAuthenticationCountAdapter',
        'staticsTrialBalance'=>
            'Sdk\Statistical\Adapter\StaticsTrialBalanceCountAdapter',
        'staticsMaxVoucherNumber'=>
            'Sdk\Statistical\Adapter\StaticsMaxVoucherNumberAdapter',
        'staticsFinalExamination'=>
            'Sdk\Statistical\Adapter\StaticsFinalExaminationAdapter',
        'staticsVoucherProfitAndLossSubject'=>
            'Sdk\Statistical\Adapter\StaticsVoucherProfitAndLossSubjectAdapter',
        'staticsEnterpriseLoanProductInfoCount'=>
            'Sdk\Statistical\Adapter\StaticsEnterpriseLoanProductInfoCountAdapter',
        'staticsFinalCheckoutStatus'=>
            'Sdk\Statistical\Adapter\StaticsFinalCheckoutStatusAdapter',
        'staticsBalanceSheet'=>
            'Sdk\Statistical\Adapter\StaticsBalanceSheetAdapter',
        'staticsIncomeStatement'=>
            'Sdk\Statistical\Adapter\StaticsIncomeStatementAdapter',
        'staticsCashFlowStatement'=>
            'Sdk\Statistical\Adapter\StaticsCashFlowStatementAdapter',
        'staticsServiceCategory'=>
            'Sdk\Statistical\Adapter\StaticsServiceCategoryAdapter',
        'staticsPolicyByMonth'=>
            'Sdk\Statistical\Adapter\StaticsPolicyByMonthAdapter',
        'staticsByJoinByMonth'=>
            'Sdk\Statistical\Adapter\StaticsByJoinByMonthAdapter',
        'staticsServiceAndFinanceByMonth'=>
            'Sdk\Statistical\Adapter\StaticsServiceAndFinanceByMonthAdapter',
        'staticsServiceAndFinanceCount'=>
            'Sdk\Statistical\Adapter\StaticsServiceAndFinanceCountAdapter',
        'staticsByAmountTransaction'=>
            'Sdk\Statistical\Adapter\StaticsByAmountTransactionAdapter',
        'staticsByServiceOrder'=>
            'Sdk\Statistical\Adapter\StaticsByServiceOrderAdapter',
        'staticsBusinessNotice'=>
            'Sdk\Statistical\Adapter\StaticsBusinessNoticeAdapter',
        'staticsCommodityScore'=>
            'Sdk\Statistical\Adapter\StaticsCommodityScoreAdapter',
        'staticsEnterpriseScore'=>
            'Sdk\Statistical\Adapter\StaticsEnterpriseScoreAdapter',
        'staticsCollectionByMember'=>
            'Sdk\Statistical\Adapter\StaticsCollectionByMemberAdapter',
        'staticsCommodityByEnterprise'=>
            'Sdk\Statistical\Adapter\StaticsCommodityByEnterpriseAdapter',
        'staticsCollectionByCategoryOrDetail'=>
            'Sdk\Statistical\Adapter\StaticsCollectionByCategoryOrDetailAdapter',
        'staticsTagCategoryUsageCount'=>
            'Sdk\Statistical\Adapter\StaticsTagCategoryUsageCountAdapter',
        'staticsTagUsageCount'=>
            'Sdk\Statistical\Adapter\StaticsTagUsageCountAdapter',
        'staticsFinishOrderByEnterprise'=>
            'Sdk\Statistical\Adapter\StaticsFinishOrderByEnterpriseAdapter',
        'staticsViewPagesAndShareVolume'=>
            'Sdk\Statistical\Adapter\StaticsViewPagesAndShareVolumeAdapter',
        'staticsPolicyRelationFinanceProduct'=>
            'Sdk\Statistical\Adapter\StaticsPolicyRelationFinanceProductAdapter',
        'staticsCourseRequirementCount'=>
            'Sdk\Statistical\Adapter\StaticsCourseRequirementCountAdapter',
        'staticsAreaChildrenCountByLevelAndId'=>
            'Sdk\Statistical\Adapter\StaticsAreaChildrenCountByLevelAndIdAdapter'
    );

    public function getAdapter(string $type) : IStatisticalAdapter
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : false;
    }
}
