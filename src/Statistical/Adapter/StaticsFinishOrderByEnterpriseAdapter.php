<?php
namespace Sdk\Statistical\Adapter;


use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Sdk\Statistical\Model\NullStatistical;

class StaticsFinishOrderByEnterpriseAdapter  extends GuzzleAdapter implements IStatisticalAdapter
{
    use StatisticalAdapterTrait;

    public function analyse(array $filter = array())
    {
        $this->get(
            $this->getResource() . '/staticsFinishOrderByEnterprise',
            array(
                'filter'=>$filter,
                'page'=>array('size'=>$filter['size'], 'number'=>$filter['number'])
            )
        );

        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
