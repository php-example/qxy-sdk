<?php
namespace Sdk\Dictionary\Repository;

use Sdk\Dictionary\Adapter\Dictionary\DictionaryRestfulAdapter;
use Sdk\Dictionary\Adapter\Dictionary\DictionaryMockAdapter;
use Sdk\Dictionary\Adapter\Dictionary\IDictionaryAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

class DictionaryRepository extends Repository implements IDictionaryAdapter
{
    use OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;
    
    private $adapter;

    const LIST_MODEL_UN = 'DICTIONARY_LIST';
    const FETCH_ONE_MODEL_UN = 'DICTIONARY_FETCH_ONE';
    
    public function __construct()
    {
        $this->adapter = new DictionaryRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }
    
    public function getActualAdapter() : IDictionaryAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IDictionaryAdapter
    {
        return new DictionaryMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
