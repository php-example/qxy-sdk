<?php
namespace Sdk\Dictionary\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Crew\Model\Crew;

use Sdk\Dictionary\Repository\DictionaryRepository;

class Dictionary implements IObject, IOperatAble, IEnableAble
{
    use OperatAbleTrait, EnableAbleTrait, Object;

    const CATEGORY = array(
        'NULL' => 0, //未知
        'NEWS' => 1, //新闻分类
        'QA' => 2, //问答分类
        'PROFESSIONAL_TITLE' => 3, //职称
        'APPLICABLE_OBJECTS' => 4, //政策适用对象
        'APPLICABLE_INDUSTRIES' => 5, //政策适用行业
        'LEVEL' => 6, //政策级别
        'CLASSIFIES' => 7, //政策分类
        'ACCOUNT_TEMPLATE_INDUSTRY' => 8, //账套行业
        'GUARANTY_STYLE' => 9, //担保方式
        'LOAN_PURPOSE' => 10, //贷款用途
        'APPOINTMENT_REJECT_REASON' => 11, //贷款申请驳回原因
        'COLLATERAL' => 12 //抵押物
    );

    private $id;

    private $name;

    private $remark;

    private $parentId;

    private $category;

    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->remark = '';
        $this->parentId = 0;
        $this->category = self::CATEGORY['NULL'];
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new DictionaryRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->remark);
        unset($this->parentId);
        unset($this->category);
        unset($this->crew);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }
    
    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setCategory(int $category) : void
    {
        $this->category =
            in_array($category, self::CATEGORY) ?
            $category : self::CATEGORY['NULL'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setParentId(int $parentId) : void
    {
        $this->parentId = $parentId;
    }

    public function getParentId() : int
    {
        return $this->parentId;
    }

    protected function getRepository() : DictionaryRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }
}
