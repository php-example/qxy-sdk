<?php
namespace Sdk\Dictionary\Model;

class DictionaryModelFactory
{
    /**
     * 所属分类
     */
    const CATEGORY = array(
            Dictionary::CATEGORY['NEWS'] => array(
                'name' => '新闻类',
                'level' => 1
            ),
            Dictionary::CATEGORY['QA'] => array(
                'name' => '问答类',
                'level' => 1
            ),
            Dictionary::CATEGORY['PROFESSIONAL_TITLE'] => array(
                'name' => '职称类',
                'level' => 3
            ),
            Dictionary::CATEGORY['APPLICABLE_OBJECTS'] => array(
                'name' => '政策适用对象',
                'level' => 1
            ),
            Dictionary::CATEGORY['APPLICABLE_INDUSTRIES'] => array(
                'name' => '政策适用行业',
                'level' => 1
            ),
            Dictionary::CATEGORY['LEVEL'] => array(
                'name' => '政策级别',
                'level' => 1
            ),
            Dictionary::CATEGORY['CLASSIFIES'] => array(
                'name' => '政策分类',
                'level' => 1
            ),
            Dictionary::CATEGORY['ACCOUNT_TEMPLATE_INDUSTRY'] => array(
                'name' => '帐套行业',
                'level' => 1
            ),
            Dictionary::CATEGORY['GUARANTY_STYLE'] => array(
                'name' => '担保方式',
                'level' => 2
            ),
            Dictionary::CATEGORY['LOAN_PURPOSE'] => array(
                'name' => '贷款用途',
                'level' => 1
            ),
            Dictionary::CATEGORY['APPOINTMENT_REJECT_REASON'] => array(
                'name' => '贷款申请驳回原因',
                'level' => 1
            ),
        );
}
