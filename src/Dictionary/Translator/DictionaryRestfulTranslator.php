<?php
namespace Sdk\Dictionary\Translator;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Model\NullDictionary;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Crew\Translator\CrewRestfulTranslator;

class DictionaryRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $dictionary = null)
    {
         return $this->translateToObject($expression, $dictionary);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $dictionary = null)
    {
        if (empty($expression)) {
            return NullDictionary::getInstance();
        }

        if ($dictionary == null) {
            $dictionary = new Dictionary();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $dictionary->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $dictionary->setName($attributes['name']);
        }
        if (isset($attributes['remark'])) {
            $dictionary->setRemark($attributes['remark']);
        }
        if (isset($attributes['parentId'])) {
            $dictionary->setParentId($attributes['parentId']);
        }
        if (isset($attributes['category'])) {
            $dictionary->setCategory($attributes['category']);
        }
        if (isset($attributes['createTime'])) {
            $dictionary->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $dictionary->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $dictionary->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $dictionary->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $dictionary->setCrew(
                $this->getCrewRestfulTranslator()->arrayToObject($crew)
            );
        }

        return $dictionary;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($dictionary, array $keys = array())
    {
        if (!$dictionary instanceof Dictionary) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'remark',
                'parentId',
                'category',
                'crew',
                'status'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'dictionaries'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $dictionary->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $dictionary->getName();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $dictionary->getRemark();
        }
        if (in_array('parentId', $keys)) {
            $attributes['parentId'] = $dictionary->getParentId();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $dictionary->getCategory();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $dictionary->getStatus();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $dictionary->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
