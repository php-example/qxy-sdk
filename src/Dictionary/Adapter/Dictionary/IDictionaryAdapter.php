<?php
namespace Sdk\Dictionary\Adapter\Dictionary;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

interface IDictionaryAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IEnableAbleAdapter
{
}
