<?php
namespace Sdk\Dictionary\Adapter\Dictionary;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Model\NullDictionary;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

class DictionaryRestfulAdapter extends GuzzleAdapter implements IDictionaryAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'DICTIONARY_LIST'=>[
            'fields'=>[
                'dictionaries'=>
                    'name,parentId,category,status,remark'
            ],
            'include'=>'crew'
        ],
        'DICTIONARY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'crew'
        ]
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new DictionaryRestfulTranslator();
        $this->resource = 'dictionaries';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => DICTIONARY_NAME_IS_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullDictionary::getInstance());
    }

    protected function addAction(Dictionary $dictionary) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $dictionary,
            array(
                'name',
                'remark',
                'category',
                'parentId',
                'crew'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($dictionary);
            return true;
        }

        return false;
    }

    protected function editAction(Dictionary $dictionary) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $dictionary,
            array(
                'name',
                'remark',
                'parentId'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$dictionary->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($dictionary);
            return true;
        }

        return false;
    }
}
