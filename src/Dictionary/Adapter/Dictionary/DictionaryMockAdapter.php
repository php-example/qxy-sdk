<?php
namespace Sdk\Dictionary\Adapter\Dictionary;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;
use Sdk\Common\Adapter\EnableAbleMockAdapterTrait;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Dictionary\Utils\MockFactory;

class DictionaryMockAdapter implements IDictionaryAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;
    
    public function fetchOne($id)
    {
        return MockFactory::generateDictionaryObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $dictionaryList = array();

        foreach ($ids as $id) {
            $dictionaryList[] = MockFactory::generateDictionaryObject($id);
        }

        return $dictionaryList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateDictionaryObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateDictionaryObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
