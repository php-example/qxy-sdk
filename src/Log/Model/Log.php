<?php
namespace Sdk\Log\Model;

use Marmot\Core;
use Marmot\Framework\Classes\Server;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class Log
{
    const TYPE = array(
        'CREW' => 1,
        'MEMBER' => 2
    );

    //日志操作标识
    private $operation;

    //日志类型标识
    private $category;

    //日志操作对象id
    private $recordObjId;

    //日志操作用户
    private $user;

    //日志链id
    private $requestId;

    //日志操作时间
    private $date;

    //企业
    private $enterprise;

    //信息
    private $message;

    private $type;

    private $ip;
    //日志id
    private $id;

    public function __construct(
        int $operation = ILogAble::OPERATION['OPERATION_NULL'],
        int $category = ILogAble::CATEGORY['NULL'],
        $recordObjId = 0,
        $type = self::TYPE['CREW'],
        $user = null,
        string $message = '',
        $enterprise = 0
    ) {
        $this->operation = $operation;
        $this->category = $category;
        $this->recordObjId = $recordObjId;
        $this->message = $message;
        $this->enterprise = $enterprise;
        $this->requestId = Server::get('REQUEST_ID');
        $this->type = $type;
        $this->user = $user;
        $this->date = '';
        $this->ip = '';
        $this->id = '';
    }

    public function __destruct()
    {
        unset($this->operation);
        unset($this->category);
        unset($this->recordObjId);
        unset($this->message);
        unset($this->requestId);
        unset($this->enterprise);
        unset($this->type);
        unset($this->user);
        unset($this->date);
        unset($this->ip);
        unset($this->id);
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function setEnterprise($enterprise)
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise()
    {
        return $this->enterprise;
    }


    public function getMessage() : string
    {
        return $this->message;
    }

    public function getOperation() : int
    {
        return $this->operation;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setType(int $type)
    {
        $this->type = $type;
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setUser(IRecordLogAble $user)
    {
        $this->user = $user;
    }

    public function getUser() : IRecordLogAble
    {
        return $this->user;
    }

    public function getRecordObjId() : int
    {
        return $this->recordObjId;
    }

    public function getRequestId() : string
    {
        return $this->requestId;
    }

    public function setDate(string $date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }
    
    public function setIp(string $esIp)
    {
        $this->ip = $esIp;
    }

    public function getIp() : string
    {
        return $this->ip;
    }
}
