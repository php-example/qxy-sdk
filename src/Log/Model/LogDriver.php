<?php
namespace Sdk\Log\Model;

use Marmot\Core;
use Monolog\Logger;

use System\Extension\Monolog\FluentdHandler;

use Sdk\Log\Model\ILogAble;
use Sdk\Log\Translator\LogMonologTranslator;

class LogDriver
{
    const LOGGER_NAME = 'command';

    private $driver;

    private $translator;

    public function __construct()
    {
        $this->driver= new Logger(self::LOGGER_NAME);
        $this->driver->pushHandler(
            new FluentdHandler(
                Core::$container->get('fluentd.address'),
                Core::$container->get('fluentd.port'),
                Logger::INFO,
                Core::$container->get('log.info.tag')
            )
        );
        $this->driver->pushHandler(
            new FluentdHandler(
                Core::$container->get('fluentd.address'),
                Core::$container->get('fluentd.port'),
                Logger::ERROR,
                Core::$container->get('log.info.tag')
            )
        );

        $this->translator = new LogMonologTranslator();
    }

    protected function getDriver()
    {
        return $this->driver;
    }

    protected function getTranslator() : LogMonologTranslator
    {
        return $this->translator;
    }

    public function info($logAble)
    {
        if ($logAble instanceof ILogAble) {
            $log = $logAble->getLog();
            $this->getDriver()->info(
                $log->getMessage(),
                $this->getTranslator()->objectToArray($log)
            );
        }
    }

    public function error($logAble)
    {
        if ($logAble instanceof ILogAble) {
            $log = $logAble->getLog();
            $this->getDriver()->error(
                $log->getMessage(),
                $this->getTranslator()->objectToArray($log)
            );
        }
    }
}
