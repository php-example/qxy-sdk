<?php
namespace Sdk\Log\Repository;

use Sdk\Log\Adapter\Log\ILogAdapter;
use Sdk\Log\Adapter\Log\LogEsAdapter;

use Sdk\Common\Repository\FetchRepositoryTrait;

class LogRepository implements ILogAdapter
{
    use FetchRepositoryTrait;

    private $adapter;

    public function __construct()
    {
        $this->adapter = new LogEsAdapter();
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }
    
    //日志管理-OA操作模块统计
    public function logCrewModularOperation($filter)
    {
        return $this->getAdapter()->logCrewModularOperation($filter);
    }

    //日志管理-OA发布内容统计
    public function logCrewRelease($filter)
    {
        return $this->getAdapter()->logCrewRelease($filter);
    }
    
    //日志管理-门户日志分析操作模块统计
    public function logUserModularOperation($filter)
    {
        return $this->getAdapter()->logUserModularOperation($filter);
    }

    //日志管理-门户实时访客统计
    public function logUserPageView($filter)
    {
        return $this->getAdapter()->logUserPageView($filter);
    }

    //日志管理-个人信息模块活跃度统计
    public function logPersonalModularOperation($filter)
    {
        return $this->getAdapter()->logPersonalModularOperation($filter);
    }
}
