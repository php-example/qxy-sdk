<?php
namespace Sdk\Log\Adapter\Log;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Log\Translator\LogEsTranslator;

use Sdk\Crew\Model\NullCrew;
use Sdk\Crew\Repository\CrewRepository;
use Sdk\Member\Model\NullMember;
use Sdk\Member\Repository\MemberRepository;

use Elasticsearch\ClientBuilder;

/**
 * @SuppressWarnings(PHPMD)
 */
class LogEsAdapter implements ILogAdapter
{
    const NDRC = 1;

    private $index;

    private $type;

    private $host;

    private $translator;

    private $clientBuilder;

    private $crewRepository;

    private $memberRepository;

    public function __construct()
    {
        $this->index = Core::$container->get('elasticsearch.index');
        $this->type = Core::$container->get('elasticsearch.type');
        $this->host = Core::$container->get('elasticsearch.hosts');
        $this->translator = new LogEsTranslator();
        $this->clientBuilder = new ClientBuilder();
        $this->crewRepository = new CrewRepository();
        $this->memberRepository = new MemberRepository();
    }

    public function __destruct()
    {
        unset($this->index);
        unset($this->type);
        unset($this->host);
        unset($this->translator);
        unset($this->clientBuilder);
        unset($this->crewRepository);
        unset($this->memberRepository);
    }

    protected function getIndex()
    {
        return $this->index;
    }

    protected function getType()
    {
        return $this->type;
    }
    
    protected function getHost()
    {
        return $this->host;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getClientBuilder() : ClientBuilder
    {
        return $this->clientBuilder;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getMemberRepository() : MemberRepository
    {
        return $this->memberRepository;
    }

    protected function getClient()
    {
        $client = $this->getClientBuilder()->create()
                       ->setHosts($this->getHost())
                       ->build();

        return $client;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function fetchOne($id)
    {
        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'id' => $id,
        ];
        
        $data = $this->getClient()->get($params)['hits'];
        
        $log = $this->getTranslator()->arrayToObject((array)$data);

        $type = $log->getType();
        $userId = $log->getUser()->getId();

        if ($type == Log::TYPE['CREW']) {
            $user = $this->getCrewRepository()->scenario(CrewRepository::FETCH_ONE_MODEL_UN)->fetchOne($userId);
        }
        if ($type == Log::TYPE['MEMBER']) {
            $user = $this->getMemberRepository()->scenario(MemberRepository::FETCH_ONE_MODEL_UN)->fetchOne($userId);
        }

        $log->setUser($user);

        return $log;
    }

    public function fetchList(array $ids) : array
    {
        unset($ids);

        $logs = array();
        
        return $logs;
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        $form = ($number-1)*$size;
        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'from' => $form,
            'size' => $size,
        ];
        
        $params['body']['sort'] = array(
            'datetime' => array(
                'order' => $sort['sort'],
            )
        );

        if (!empty($filter['userId'])) {
            $params['body']['query']['bool']['must'][]['match']['context.user.id'] = (int)$filter['userId'];
        }

        if (!empty($filter['type'])) {
            $params['body']['query']['bool']['must'][]['match']['context.type'] = (int)$filter['type'];
        }

        if (!empty($filter['enterprise'])) {
            $params['body']['query']['bool']['must'][]['match']['context.enterprise'] = (int)$filter['enterprise'];
        }

        if (!empty($filter['operation'])) {
            $params['body']['query']['bool']['must'][]['match']['context.operation'] = (int)$filter['operation'];
        }
        
        if (!empty($filter['startTime'])) {
            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $filter['startTime'];
        }

        if (!empty($filter['endTime'])) {
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $filter['endTime'];
        }

        $cursor = $this->getClient()->search($params)['hits'];

        $list = $cursor['hits'];

        $total = $cursor['total'];
        
        $logs = array();
      
        foreach ($list as $document) {
            $logs[] = $this->getTranslator()->arrayToObject((array)$document);
        }
        
        $crewIds = array();
        $memberIds = array();

        foreach ($logs as $log) {
            if ($log->getType() == Log::TYPE['CREW']) {
                $crewIds[] = $log->getUser()->getId();
            }

            if ($log->getType() == Log::TYPE['MEMBER']) {
                $memberIds[] = $log->getUser()->getId();
            }
        }

        $crewIds = array_unique($crewIds);
        $memberIds = array_unique($memberIds);

        $memberList = array();
        $crewList = array();

        list($count, $members) = $this->getMemberRepository()->fetchList($memberIds);
        list($count, $crews) = $this->getCrewRepository()->fetchList($crewIds);
        unset($count);

        foreach ($members as $member) {
            $memberList[$member->getId()] = $member;
        }

        foreach ($crews as $crew) {
            $crewList[$crew->getId()] = $crew;
        }

        foreach ($logs as $log) {
            if ($log->getType() == Log::TYPE['CREW']) {
                $crewId = $log->getUser()->getId();
                $crew = isset($crewList[$crewId]) ? $crewList[$crewId] : NullCrew::getInstance();
                $log->setUser($crew);
            }
            if ($log->getType() == Log::TYPE['MEMBER']) {
                $memberId = $log->getUser()->getId();
                $member = isset($memberList[$memberId]) ? $memberList[$memberId] : NullMember::getInstance();
                $log->setUser($member);
            }
        }

        return [$total, $logs];
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     * 日志管理-OA操作模块统计
     * 设置默认的模块分类$defaultCategory
     * 设置类型=1
     * 根据不同的时间类型,设置不同的时间搜索条件
     * 聚合: 先通过模块分类分组,在通过操作类型分组
     * 操作模块统计
     */
    public function logCrewModularOperation($filter)
    {
        $defaultCategory = array(
            ILogAble::CATEGORY['NATURAL_PERSON'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            ILogAble::CATEGORY['ENTERPRISE'],
            ILogAble::CATEGORY['FINANCE_AUTHENTICATION'],
            ILogAble::CATEGORY['PROFESSIONAL_TITLE'],
            ILogAble::CATEGORY['SERVICE'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            ILogAble::CATEGORY['LOAN_PRODUCT']
        );
    
        $defaultOperation = array(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::OPERATION['OPERATION_REJECT']
        );

        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'size' => 0,
            'body' =>[
                "aggs" => [
                    "category" => [
                        "terms" => [
                            "field" => "context.category",
                            "include" => $defaultCategory
                        ],
                        "aggs" => [
                            "operation" => [
                                "terms" => [
                                    "field" => "context.operation",
                                    "include" => $defaultOperation
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $params['body']['query']['bool']['must'][]['match']['context.type'] = Log::TYPE['CREW'];

        $timeType1 = $filter['timeType'];

        if ($timeType1 == ILogAble::TIME_TYPE['DAY']) {
            $params['body']['query']['bool']['must'][]['match']['datetime'] = date("Y-m-d");
        }

        if ($timeType1 == ILogAble::TIME_TYPE['WEEK']) {
            $week = $this->getWeekMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $week['weekStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $week['weekEnd'];
        }

        if ($timeType1 == ILogAble::TIME_TYPE['MONTH']) {
            $month = $this->getMonthMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $month['monthStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $month['monthEnd'];
        }

        $response = $this->getClient()->search($params);

        return $response;
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     * 日志管理-OA发布内容统计
     * 发布政策、关联解读、发布新闻
     */
    public function logCrewRelease($filter)
    {
        $defaultCategory = array(
            ILogAble::CATEGORY['POLICY'],
            ILogAble::CATEGORY['POLICY_INTERPRETATION'],
            ILogAble::CATEGORY['MONEY_WISE_NEWS'],
            ILogAble::CATEGORY['POLICY_PRODUCT']
        );

        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'size' => 0,
            'body' =>[
                "aggs" => [
                    "category" => [
                        "terms" => [
                            "field" => "context.category",
                            "include" => $defaultCategory
                        ]
                    ]
                ]
            ]
        ];

        $params['body']['query']['bool']['must'][]['match']['context.type'] = Log::TYPE['CREW'];
        $params['body']['query']['bool']['must'][]['match']['context.operation'] = ILogAble::OPERATION['OPERATION_ADD'];

        $timeType2 = $filter['timeType'];

        if ($timeType2 == ILogAble::TIME_TYPE['DAY']) {
            $params['body']['query']['bool']['must'][]['match']['datetime'] = date("Y-m-d");
        }

        if ($timeType2 == ILogAble::TIME_TYPE['WEEK']) {
            $week = $this->getWeekMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $week['weekStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $week['weekEnd'];
        }

        if ($timeType2 == ILogAble::TIME_TYPE['MONTH']) {
            $month = $this->getMonthMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $month['monthStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $month['monthEnd'];
        }
        
        $response = $this->getClient()->search($params);

        return $response;
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     * 日志管理-门户日志分析操作模块统计
     */
    public function logUserModularOperation($filter)
    {
        $defaultCategory = array(
            ILogAble::CATEGORY['SAFETY'],
            ILogAble::CATEGORY['INFORMATION'],
            ILogAble::CATEGORY['ENTERPRISE'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            ILogAble::CATEGORY['MEMBER_ACCOUNT'],
            ILogAble::CATEGORY['DELIVERY_ADDRESS'],
            ILogAble::CATEGORY['COUPON'],
            ILogAble::CATEGORY['ORDER'],
            ILogAble::CATEGORY['WORKBENCH_ORDER'],
            ILogAble::CATEGORY['FINANCIAL_ANSWER'],
            ILogAble::CATEGORY['APPOINTMENT'],
            ILogAble::CATEGORY['FINANCIAL_QUESTION'],
            ILogAble::CATEGORY['FINANCE_AUTHENTICATION'],
            ILogAble::CATEGORY['NATURAL_PERSON'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            ILogAble::CATEGORY['SERVICE'],
            ILogAble::CATEGORY['LOAN_PRODUCT'],
            ILogAble::CATEGORY['WORKBENCH_APPOINTMENT']
        );
        
        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'size' => 0,
            'body' =>[
                "aggs" => [
                    "category" => [
                        "terms" => [
                            "field" => "context.category",
                            "include" => $defaultCategory
                        ]
                    ]
                ]
            ]
        ];
    
        $params['body']['query']['bool']['must'][]['match']['context.type'] = Log::TYPE['MEMBER'];

        if (!empty($filter['userId'])) {
            $params['body']['query']['bool']['must'][]['match']['context.user.id'] = $filter['userId'];
        }
        
        $timeType3 = $filter['timeType'];

        if ($timeType3 == ILogAble::TIME_TYPE['DAY']) {
            $params['body']['query']['bool']['must'][]['match']['datetime'] = date("Y-m-d");
        }

        if ($timeType3 == ILogAble::TIME_TYPE['WEEK']) {
            $week = $this->getWeekMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $week['weekStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $week['weekEnd'];
        }

        if ($timeType3 == ILogAble::TIME_TYPE['MONTH']) {
            $month = $this->getMonthMyActionAndEnd();
            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $month['monthStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $month['monthEnd'];
        }
        
        $response = $this->getClient()->search($params);

        return $response;
    }

    /**
     * @SuppressWarnings(PHPMD)
     * 日志管理-门户实时访客统计
     */
    public function logUserPageView($filter)
    {
        $operation = array(
            ILogAble::OPERATION['OPERATION_SIGN_IN']
        );

        $timeType4 = $filter['timeType'];

        if ($timeType4 == ILogAble::TIME_TYPE['DAY']) {
            $params = [
                'index' => $this->getIndex(),
                'type' => $this->getType(),
                "size" => 0,
                'body' =>[
                    'aggs' => [
                        'dateTime' => [
                            "date_histogram" => [
                                "field" => "datetime",
                                "interval" => "2h",
                                "format" => "HH:mm",
                                "min_doc_count" => 0,
                            ],
                            "aggs" => [
                                "operation" => [
                                    "terms" => [
                                        "field" => "context.operation",
                                        "include" => $operation
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $params['body']['query']['bool']['must'][]['match']['datetime'] = date("Y-m-d");
        }

        if ($timeType4 == ILogAble::TIME_TYPE['WEEK']) {
            $week = $this->getWeekMyActionAndEnd();
            $params = [
                'index' => $this->getIndex(),
                'type' => $this->getType(),
                "size" => 0,
                'body' =>[
                    'aggs' => [
                        'dateTime' => [
                            "date_histogram" => [
                                "field" => "datetime",
                                "interval" => "day",
                                "format" => "yyyy-MM-dd",
                                "min_doc_count" => 0,
                                "extended_bounds" => [
                                    "min" => $week['weekStart'],
                                    "max" => $week['weekEnd']
                                ]
                            ],
                            "aggs" => [
                                "operation" => [
                                    "terms" => [
                                        "field" => "context.operation",
                                        "include" => $operation
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if ($timeType4 == ILogAble::TIME_TYPE['MONTH']) {
            $month = $this->getMonthMyActionAndEnd();
            $params = [
                'index' => $this->getIndex(),
                'type' => $this->getType(),
                "size" => 0,
                'body' =>[
                    'aggs' => [
                        'dateTime' => [
                            "date_histogram" => [
                                "field" => "datetime",
                                "interval" => "day",
                                "format" => "yyyy-MM-dd",
                                "min_doc_count" => 0,
                                "extended_bounds" => [
                                    "min" => $month['monthStart'],
                                    "max" => $month['monthEnd']
                                ]
                            ],
                            "aggs" => [
                                "operation" => [
                                    "terms" => [
                                        "field" => "context.operation",
                                        "include" => $operation
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        $params['body']['query']['bool']['must'][]['match']['context.type'] = Log::TYPE['MEMBER'];

        $response = $this->getClient()->search($params);

        return $response;
    }

    //个人信息活跃度统计
    //设置默认的模块分类$defaultCategory
    //设置类型=1
    //根据不同的时间类型,设置不同的时间搜索条件
    //聚合: 先通过模块分类分组,在通过操作类型分组
    public function logPersonalModularOperation($filter)
    {
        $defaultCategory = array(
            ILogAble::CATEGORY['NATURAL_PERSON'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            ILogAble::CATEGORY['ENTERPRISE'],
            ILogAble::CATEGORY['FINANCE_AUTHENTICATION'],
            ILogAble::CATEGORY['PROFESSIONAL_TITLE'],
            ILogAble::CATEGORY['SERVICE'],
            ILogAble::CATEGORY['LOAN_PRODUCT'],
            ILogAble::CATEGORY['POLICY'],
            ILogAble::CATEGORY['DISPATCH_DEPARTMENT'],
            ILogAble::CATEGORY['SERVICE_CATEGORY'],
            ILogAble::CATEGORY['ORDER'],
            ILogAble::CATEGORY['WORKBENCH_ORDER'],
            ILogAble::CATEGORY['WORKBENCH_APPOINTMENT'],
            ILogAble::CATEGORY['FINANCIAL_ANSWER'],
            ILogAble::CATEGORY['MONEY_WISE_NEWS'],
            ILogAble::CATEGORY['MEMBER'],
            ILogAble::CATEGORY['MEMBER_ACCOUNT'],
            ILogAble::CATEGORY['PLATFORM_COUPON'],
            ILogAble::CATEGORY['DICTIONARY']
        );
    
        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'size' => 0,
            'body' =>[
                "aggs" => [
                    "category" => [
                        "terms" => [
                            "field" => "context.category",
                            "include" => $defaultCategory
                        ]
                    ]
                ]
            ]
        ];

        $params['body']['query']['bool']['must'][]['match']['context.type'] = Log::TYPE['CREW'];

        $userId = Core::$container->get('crew')->getId();
        $params['body']['query']['bool']['must'][]['match']['context.user.id'] = $userId;

        $timeType = $filter['timeType'];

        if ($timeType == ILogAble::TIME_TYPE['DAY']) {
            $params['body']['query']['bool']['must'][]['match']['datetime'] = date("Y-m-d");
        }

        if ($timeType == ILogAble::TIME_TYPE['WEEK']) {
            $week = $this->getWeekMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $week['weekStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $week['weekEnd'];
        }

        if ($timeType == ILogAble::TIME_TYPE['MONTH']) {
            $month = $this->getMonthMyActionAndEnd();

            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $month['monthStart'];
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $month['monthEnd'];
        }
    
        $response = $this->getClient()->search($params);

        return $response;
    }

    /*
    * 获取某星期的开始时间和结束时间
    * time 时间
    * first 表示每周星期一为开始日期 0表示每周日为开始日期
    */
    protected function getWeekMyActionAndEnd($time = '', $first = 1)
    {
        //当前日期
        if (!$time) {
            $time = time();
        }
        $defaultDate = date("Y-m-d h:i:s", $time);
        //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $week = date('w', strtotime($defaultDate));
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $weekStart = date('Y-m-d', strtotime("$defaultDate -" . ($week ? $week - $first : 6) . ' days'));
        //本周结束日期
        $weekEnd = date('Y-m-d', strtotime("$weekStart +6 days"));

        return array("weekStart" => $weekStart, "weekEnd" => $weekEnd);
    }

    protected function getMonthMyActionAndEnd($date = '')
    {
        //当前日期
        if (!$date) {
            $date = date("Y-m-d");
        }

        $monthStart = date('Y-m-01', strtotime($date));
        $monthEnd = date('Y-m-d', strtotime("$monthStart +1 month -1 day"));

        return array("monthStart" => $monthStart, "monthEnd" => $monthEnd);
    }

    protected function getYearMyActionAndEnd($year = 0)
    {
        if (empty($year)) {
            $year = date('Y', time());
        }
      
        $emonth = 12;
        $startTime = $year.'-01'.'-01';
        $endm = $year.'-'.$emonth.'-1';
        $endTime = date('Y-m-t', strtotime($endm));

        return array('yearStart'=> $startTime,'yearEnd'=> $endTime);
    }
}
