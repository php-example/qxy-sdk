<?php
namespace Sdk\Log\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Server;
use Sdk\Log\Model\NullLog;

class LogMonologTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $log = null)
    {
        unset($expression);
        unset($log);
        return new NullLog();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($log, array $keys = array())
    {
        unset($keys);
        return [
            'operation' => $log->getOperation(),
            'category' => $log->getCategory(),
            'request_id' => $log->getRequestId(),
            'obj_id' => $log->getRecordObjId(),
            'ip' => Server::get('REMOTE_ADDR'),
            'type' => $log->getType(),
            'user' => [
                'id'=>$log->getUser()->getId()
            ],
            'enterprise' => $log->getEnterprise()
        ];
    }
}
