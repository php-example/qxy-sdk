<?php
namespace Sdk\Log\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Log\Model\Log;
use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;
use Sdk\Member\Model\Member;
use Sdk\Common\Translator\RestfulTranslatorTrait;

class LogEsTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $log = null)
    {
        $context = (array)$expression['_source']['context'];

        $enterprise = isset($context['enterprise']) ? $context['enterprise'] : 0;

        $log = new Log(
            $context['operation'],
            $context['category'],
            $context['obj_id'],
            $context['type'],
            $context['user'],
            $expression['_source']['message'],
            $enterprise
        );

        $log->setId((string)$expression['_id']);
        $log->setDate($expression['_source']['datetime']);
        
        if (isset($context['type'])) {
            $log->setType($context['type']);
        }
        if (isset($context['ip'])) {
            $log->setIp($context['ip']);
        }
        if (isset($context['enterprise'])) {
            $log->setEnterprise($context['enterprise']);
        }

        $userContext = (array)$context['user'];

        $user = new NullCrew();
        
        if ($context['type'] == Log::TYPE['CREW']) {
            $user = new Crew($userContext['id']);
        }
        if ($context['type'] == Log::TYPE['MEMBER']) {
            $user = new Member($userContext['id']);
        }

        $log->setUser($user);

        return $log;
    }

    public function arrayToObjects(array $expression) : array
    {
        $logs = array();

        foreach ($expression as $each) {
            $logs[] = $this->arrayToObject($each);
        }

        return $logs;
    }

    public function objectToArray($log, array $keys = array())
    {
        unset($log);
        unset($keys);
        return array();
    }
}
