<?php
namespace Sdk\Common\Model;

interface IRecommendAble
{
    const RECOMMEND_HOMEPAGE_STATUS = array(
        'NO' => 0,
        'YES' => 2
    );

    public function recommend() : bool;
    
    public function cancelRecommend() : bool;
}
