<?php
namespace Sdk\Common\Model;

trait NullRecommendAbleTrait
{
    public function recommend() : bool
    {
        return $this->resourceNotExist();
    }

    public function cancelRecommend() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}
