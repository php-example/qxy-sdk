<?php
namespace Sdk\Common\Model;

use Sdk\Common\Adapter\ITopAbleAdapter;

trait TopAbleTrait
{
    /**
     * 启用
     * @return bool 是否启用成功
     */
    public function top() : bool
    {
        $topAdapter = $this->getITopAbleAdapter();
        return $topAdapter->top($this);
    }

    /**
     * 禁用
     * @return bool 是否禁用成功
     */
    public function cancelTop() : bool
    {
        if (!$this->isStick()) {
            return false;
        }
        $topAdapter = $this->getITopAbleAdapter();
        return $topAdapter->cancelTop($this);
    }

    abstract protected function getITopAbleAdapter() : ITopAbleAdapter;

    public function setStick(int $stick)
    {
        $this->stick = in_array($stick, array_values(self::STICK)) ? $stick : self::STICK['DISABLED'];
    }

    public function getStick() : int
    {
        return $this->stick;
    }

    public function isStick() : bool
    {
        return $this->getStick() == self::STICK['ENABLED'];
    }
}
