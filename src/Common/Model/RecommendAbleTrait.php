<?php
namespace Sdk\Common\Model;

use Sdk\Common\Adapter\IRecommendAbleAdapter;

trait RecommendAbleTrait
{
    /**
     * 推荐
     * @return bool 是否推荐成功
     */
    public function recommend() : bool
    {
        if ($this->isRecommendStatus()) {
            return false;
        }

        $recommendAdapter = $this->getIRecommendAbleAdapter();
        return $recommendAdapter->recommend($this);
    }

    /**
     * 推荐
     * @return bool 是否推荐成功
     */
    public function cancelRecommend() : bool
    {
        if (!$this->isRecommendStatus()) {
            return false;
        }

        $recommendAdapter = $this->getIRecommendAbleAdapter();
        return $recommendAdapter->cancelRecommend($this);
    }

    abstract protected function getIRecommendAbleAdapter() : IRecommendAbleAdapter;

    public function isRecommendStatus() : bool
    {
        return $this->getRecommendStatus() == self::RECOMMEND_HOMEPAGE_STATUS['YES'];
    }
}
