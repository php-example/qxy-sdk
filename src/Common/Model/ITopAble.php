<?php
namespace Sdk\Common\Model;

interface ITopAble
{
    const STICK = array(
        'ENABLED' => 2 ,
        'DISABLED' => 0
    );

    public function top() : bool;
    
    public function cancelTop() : bool;
}
