<?php
namespace Sdk\Common\Repository;

use Sdk\Common\Model\Document;

trait DocumentRepositoryTrait
{
	public function add(Document $document) 
	{
		return $this->getAdapter()->add($document);
	}

	public function fetchOne(Document $document)
	{
		return $this->getAdapter()->fetchOne($document);
	}
}