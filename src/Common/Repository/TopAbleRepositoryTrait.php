<?php
namespace Sdk\Common\Repository;

use Sdk\Common\Model\ITopAble;

trait TopAbleRepositoryTrait
{
    public function top(ITopAble $topAbleObject) : bool
    {
        return $this->getAdapter()->top($topAbleObject);
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        return $this->getAdapter()->cancelTop($topAbleObject);
    }
}
