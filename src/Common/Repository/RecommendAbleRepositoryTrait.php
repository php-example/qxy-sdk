<?php
namespace Sdk\Common\Repository;

use Sdk\Common\Model\IRecommendAble;

trait RecommendAbleRepositoryTrait
{
    public function recommend(IRecommendAble $recommendAbleObject) : bool
    {
        return $this->getAdapter()->recommend($recommendAbleObject);
    }

    public function cancelRecommend(IRecommendAble $recommendAbleObject) : bool
    {
        return $this->getAdapter()->cancelRecommend($recommendAbleObject);
    }
}
