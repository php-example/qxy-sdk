<?php
namespace Sdk\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\Model\IRecommendAble;
use Sdk\Common\Command\RecommendCommand;

abstract class RecommendCommandHandler implements ICommandHandler
{
    abstract protected function fetchIRecommendObject($id) : IRecommendAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(RecommendCommand $command)
    {
        $this->recommendAble = $this->fetchIRecommendObject($command->id);
        
        if ($this->recommendAble->recommend()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
