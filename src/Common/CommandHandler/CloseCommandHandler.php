<?php
namespace Sdk\Common\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\Utils\LogDriverCommandHandlerTrait;

use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\Command\CloseCommand;

abstract class CloseCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;
    
    abstract protected function fetchIModifyStatusObject($id) : IModifyStatusAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(CloseCommand $command)
    {
        $this->closeAble = $this->fetchIModifyStatusObject($command->id);

        if ($this->closeAble->close()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
