<?php
namespace Sdk\Common\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\Utils\LogDriverCommandHandlerTrait;

use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Common\Command\DeleteCommand;

abstract class DeleteCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;
    
    abstract protected function fetchIModifyStatusObject($id) : IModifyStatusAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(DeleteCommand $command)
    {
        $this->deleteAble = $this->fetchIModifyStatusObject($command->id);

        if ($this->deleteAble->deletes()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
