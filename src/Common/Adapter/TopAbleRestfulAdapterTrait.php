<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\ITopAble;

trait TopAbleRestfulAdapterTrait
{
    abstract protected function getResource() : string;

    public function top(ITopAble $topAbleObject) : bool
    {
        return $this->topAction($topAbleObject);
    }

    protected function topAction(ITopAble $topAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$topAbleObject->getId().'/top'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($topAbleObject);
            return true;
        }
        return false;
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        return $this->cancelTopAction($topAbleObject);
    }

    protected function cancelTopAction(ITopAble $topAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$topAbleObject->getId().'/cancelTop'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($topAbleObject);
            return true;
        }
        return false;
    }
}
