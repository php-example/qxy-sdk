<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IRecommendAble;

trait RecommendAbleMockAdapterTrait
{
    public function recommend(IRecommendAble $recommendAbleObject) : bool
    {
        unset($recommendAbleObject);
        return true;
    }

    public function cancelRecommend(IRecommendAble $recommendAbleObject) : bool
    {
        unset($recommendAbleObject);
        return true;
    }
}
