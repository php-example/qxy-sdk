<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IRecommendAble;

trait RecommendAbleRestfulAdapterTrait
{
    abstract protected function getResource() : string;

    public function recommend(IRecommendAble $recommendAbleObject) : bool
    {
        return $this->recommendAction($recommendAbleObject);
    }

    protected function recommendAction(IRecommendAble $recommendAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$recommendAbleObject->getId().'/recommend'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($recommendAbleObject);
            return true;
        }
        return false;
    }

    public function cancelRecommend(IRecommendAble $recommendAbleObject) : bool
    {
        return $this->cancelRecommendAction($recommendAbleObject);
    }

    protected function cancelRecommendAction(IRecommendAble $recommendAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$recommendAbleObject->getId().'/cancelRecommend'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($recommendAbleObject);
            return true;
        }
        return false;
    }
}
