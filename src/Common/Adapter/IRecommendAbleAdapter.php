<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IRecommendAble;

interface IRecommendAbleAdapter
{
    public function recommend(IRecommendAble $recommendAbleObject) : bool;

    public function cancelRecommend(IRecommendAble $recommendAbleObject) : bool;
}
