<?php
namespace Sdk\Common\Adapter\Document;

class RejectReasonDocumentAdapter extends DocumentAdapter
{
    const DBNAME = 'huizhonglianhe';

    const COLLECTIONNAME = 'rejectReason';

    public function __construct()
    {
        parent::__construct(self::DBNAME, self::COLLECTIONNAME);
    }
}
