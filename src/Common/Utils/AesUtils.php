<?php
namespace Sdk\Common\Utils;

use Marmot\Core;

/**
 * @SuppressWarnings(PHPMD)
 */
class AesUtils
{
    protected $method;
    protected $secretKey;
    protected $iv;
    protected $options;

    /**
     * AesApi constructor.
     * @param $key 密码
     * return base64_encode(openssl_random_pseudo_bytes(32));
     *
     * @param string $method  加密模式  dump(openssl_get_cipher_methods());
     *  常见的AES加密方法有ECB，CTR，GMAC，GCM，相同之处都是：明文分组->加密->密文分组。不同之处在于：
     *  ECB的加密是单纯的明文分支与密钥进行加密运算，也就是说会出现有着相同明文的不同分组加密后有一样的密文分组，这一点很容易被针对从而破解出密钥
     *  CTR加密过程引入计数器概念，没一轮加密后运行一次计数器改变密钥，从而摆脱ECB的缺陷。不够这个方式需要一个初始计数器的值，也就是 $iv
     *  GMAC 加密过程引入MAC（消息验证码）来保证消息的完整性，需要一个附加消息与密文一起生成MAC值
     *  GCM G是GMAC C是CTR 两者综合
     *
     * @param int $options
     *
     *  0 : 自动对明文进行 padding, 返回的数据经过 base64 编码.
     *  1 : OPENSSL_RAW_DATA, 自动对明文进行 padding, 但返回的结果未经过 base64 编码.
     *  2 : OPENSSL_ZERO_PADDING, 自动对明文进行 0 填充, 返回的结果经过 base64 编码.
     *  但是, openssl 不推荐 0 填充的方式, 即使选择此项也不会自动进行 padding, 仍需手动 padding.
     *
     * @param string $iv  偏移量 (非空的初始化向量, 不使用此项会抛出一个警告)
     * base64_encode(openssl_random_pseudo_bytes(16));
     *
     */
    const DEFAULT_METHOD = 'aes-128-cbc';

    public function __construct($secretKey = '', $iv = '', $method = '', $options = 1)
    {
        if ($this->checkExtension()) {
            $this->method = empty($method) ? self::DEFAULT_METHOD : $method;
            $this->secretKey = $this->randCode(openssl_cipher_iv_length($this->method));
            $this->options = $options;
            $this->iv = $this->randCode(openssl_cipher_iv_length($this->method));
        }
    }
    // 是否启用了openssl扩展
    protected function checkExtension() : bool
    {
        if (extension_loaded('openssl')) {
            return true;
        }

        Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('point'=>'openssl not suport'));
        return false;
    }

    public function getSecretKey() : string
    {
        return $this->secretKey;
    }

    public function getMethod() : string
    {
        return $this->method;
    }

    public function getIv() : string
    {
        return $this->iv;
    }

    public function getOptions() : int
    {
        return $this->options;
    }

    public function setSecretKey(string $secretKey) : void
    {
        $this->secretKey = $secretKey;
    }

    public function setMethod(string $method) : void
    {
        $this->method = $method;
    }

    public function setIv(string $iv) : void
    {
        $this->iv = $iv;
    }

    public function setOptions(int $options) : void
    {
        $this->options = $options;
    }

    public function decrypt($data)
    {
        if ($this->checkExtension() && $this->validateParams()) {
            $plaintext = base64_decode($data);
            
            return openssl_decrypt(
                $plaintext,
                $this->getMethod(),
                $this->getSecretKey(),
                $this->getOptions(),
                $this->getIv()
            );
        }
        return false;
    }

    public function encrypt($data)
    {
        if ($this->checkExtension() && $this->validateParams()) {
            $result = openssl_encrypt(
                $data,
                $this->getMethod(),
                $this->getSecretKey(),
                $this->getOptions(),
                $this->getIv()
            );

            return base64_encode($result);
        }
        return false;
    }

    protected function validateParams()
    {
        if (empty($this->getMethod()) || !in_array($this->getMethod(), openssl_get_cipher_methods())) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('point'=>'aes method error'));
            return false;
        }

        if (empty($this->getIv())) {
            Core::setLastError(PARAMETER_FORMAT_INCORRECT, array('point'=>'iv error'));
            return false;
        }
        return true;
    }

    private function randCode($length = 16)
    {
        $string = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~@#$%^&*(){}[]|=-,./!`_+:;<>";

        $count = strlen($string) - 1;
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $code .= $string[rand(0, $count)];
        }
        return $code;
    }
}
