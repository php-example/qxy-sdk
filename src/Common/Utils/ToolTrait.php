<?php
namespace Sdk\Common\Utils;

use Marmot\Core;
use Marmot\Framework\Classes\Server;

/**
 * @SuppressWarnings(PHPMD)
 */
trait ToolTrait
{
    public function getHearderData(int $type) : array
    {
        $headerType = array(
            'NULL' => 0,
            'APP' => 1,
            'POTAL' => 2
        );
        $sessionId = $headerType['NULL'];
        if($type == $headerType['APP']){
            $sessionId = Server::get('REMOTE_ADDR').Core::$container->get('user')->getId();
        }
        if($type == $headerType['POTAL']){
            $sessionId = session_id();
        }
        
        $header = array(
            'session_id' => $sessionId,
            'ip' => Server::get('REMOTE_ADDR'),
            'member_id' => Core::$container->get('user')->getId()
        );

        return $header;
    }
}