<?php
namespace Sdk\Common\Utils;

use Sdk\Common\Utils\AesUtils;
use Sdk\Common\Utils\RsaUtils;

trait RsaAndAesTrait
{
    protected function getAesUtils()
    {
        return new AesUtils();
    }

    protected function getRsaUtils()
    {
        return new RsaUtils();
    }
}
