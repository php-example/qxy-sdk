<?php
namespace Sdk\Common\Utils;

class RsaUtils
{
    /**
     * RSA签名
     * @param $data 待签名数据
     * @param $privateKeyPath 商户私钥文件路径
     * return 签名结果
     */
    public function makeSign(string $data, string $privateKeyPath = '')
    {
        $privateKeyPath = empty($privateKeyPath) ? CERT_PRIVATE_PATH : $privateKeyPath;

        $priKey = file_get_contents($privateKeyPath);
        $privateKey = openssl_get_privatekey($priKey);

        openssl_sign($data, $sign, $privateKey, OPENSSL_ALGO_SHA256);

        openssl_free_key($privateKey);

        return base64_encode($sign);
    }

    /**
     * RSA验签
     * @param $data 待签名数据
     * @param $ali_public_key_path 支付宝的公钥文件路径
     * @param $sign 要校对的的签名结果
     * return 验证结果
     */
    public function verifySign(string $data, string $sign, string $publicKeyPath = '')
    {
        $publicKeyPath = empty($publicKeyPath) ? CERT_PUBLIC_PATH : $publicKeyPath;

        $pubKey = file_get_contents($publicKeyPath);
        $publicKey = openssl_get_publickey($pubKey);

        $result = (bool)openssl_verify($data, base64_decode($sign), $publicKey, OPENSSL_ALGO_SHA256);

        openssl_free_key($publicKey);

        return $result;
    }

    public function encrypt(string $content, string $publicKeyPath = '')
    {
        $publicKeyPath = empty($publicKeyPath) ? CERT_PUBLIC_PATH : $publicKeyPath;
        $pubKey = file_get_contents($publicKeyPath);
        $publicKey = openssl_get_publickey($pubKey);

        //把需要加密的内容，按256位拆开解密
        $result  = '';
        for ($i = 0; $i < strlen($content)/256; $i++) {
            $data = substr($content, $i * 256, 256);
            openssl_public_encrypt($data, $encrypt, $publicKey);
            $result .= $encrypt;
        }

        openssl_free_key($publicKey);

        return base64_encode($result);
    }
    /**
     * RSA解密
     * @param $content 需要解密的内容，密文
     * @param $private_key_path 商户私钥文件路径
     * return 解密后内容，明文
     */
    public function decrypt(string $content, string $privateKeyPath = '')
    {
        $privateKeyPath = empty($privateKeyPath) ? CERT_PRIVATE_PATH : $privateKeyPath;

        $priKey = file_get_contents($privateKeyPath);
        $res = openssl_get_privatekey($priKey);

        $content = base64_decode($content);

        //把需要解密的内容，按256位拆开解密
        $result  = '';
        for ($i = 0; $i < strlen($content)/256; $i++) {
            $data = substr($content, $i * 256, 256);
            openssl_private_decrypt($data, $decrypt, $res);
            $result .= $decrypt;
        }
        openssl_free_key($res);
        return $result;
    }
}
