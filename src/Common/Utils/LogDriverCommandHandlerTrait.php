<?php
namespace Sdk\Common\Utils;

use Sdk\Log\Model\LogDriver;

trait LogDriverCommandHandlerTrait
{
    protected function getLogDriver()
    {
        return new LogDriver();
    }

    protected function logDriverInfo($handler)
    {
        $this->getLogDriver()->info($handler);
    }

    protected function logDriverError($handler)
    {
        $this->getLogDriver()->error($handler);
    }
}
