<?php
namespace Sdk\DispatchDepartment\Adapter\DispatchDepartment;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

interface IDispatchDepartmentAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter, IAsyncAdapter //phpcs:ignore
{
}
