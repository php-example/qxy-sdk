<?php
namespace Sdk\Role\Adapter\Role;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Role\Model\Role;
use Sdk\Role\Model\NullRole;
use Sdk\Role\Translator\RoleRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class RoleRestfulAdapter extends GuzzleAdapter implements IRoleAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'ROLE_LIST'=>[
                'fields'=>[],
                'include'=> 'crew','roles','userGroup'
            ],
            'ROLE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew','roles','userGroup'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new RoleRestfulTranslator();
        $this->resource = 'roles';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => ROLE_NAME_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullRole::getInstance());
    }

    protected function addAction(Role $role) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $role,
            array(
                'name',
                'description',
                'permission',
                'status',
                'crew',
                'userGroup'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($role);
            return true;
        }

        return false;
    }
        
    protected function editAction(Role $role) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $role,
            array(
                'name',
                'description',
                'permission',
                'userGroup'
            )
        );

        $this->patch(
            $this->getResource().'/'.$role->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($role);
            return true;
        }

        return false;
    }

    public function permission(Role $role) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $role,
            array('permission')
        );

        $this->patch(
            $this->getResource().'/'.$role->getId().'/permission',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($role);
            return true;
        }

        return false;
    }
}
