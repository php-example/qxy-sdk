<?php
namespace Sdk\Role\Adapter\Role;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

use Sdk\Role\Model\Role;

interface IRoleAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter, IAsyncAdapter //phpcs:ignore
{
    public function permission(Role $permission) : bool;
}
