<?php
namespace Sdk\Role\Adapter\Role;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;
use Sdk\Common\Adapter\EnableAbleMockAdapterTrait;

use Sdk\Role\Model\Role;
use Sdk\Role\Utils\MockFactory;

class RoleMockAdapter implements IRoleAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function permission(Role $permission) : bool
    {
        unste($permission);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateRoleObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $roleList = array();

        foreach ($ids as $id) {
            $roleList[] = MockFactory::generateRoleObject($id);
        }

        return $roleList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateRoleObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateRoleObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
