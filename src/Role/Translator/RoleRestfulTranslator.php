<?php
namespace Sdk\Role\Translator;

use Sdk\Role\Model\Role;
use Sdk\Role\Model\NullRole;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\BranchOffice\Translator\BranchOfficeRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class RoleRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    protected function getBranchOfficeRestfulTranslator()
    {
        return new BranchOfficeRestfulTranslator();
    }

    public function arrayToObject(array $expression, $role = null)
    {
        return $this->translateToObject($expression, $role);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $role = null)
    {
        if (empty($expression)) {
            return NullRole::getInstance();
        }

        if ($role == null) {
            $role = new Role();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $role->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';
        
        if (isset($attributes['name'])) {
            $role->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $role->setDescription($attributes['description']);
        }
        if (isset($attributes['permission'])) {
            $role->setPermission($attributes['permission']);
        }
        if (isset($attributes['userGroupType'])) {
            $role->setUserGroupType($attributes['userGroupType']);
        }
        if (isset($attributes['createTime'])) {
            $role->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $role->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $role->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $role->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($relationships['userGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['userGroup']['data']);
            $role->setBranchOffice($this->getBranchOfficeRestfulTranslator()->arrayToObject($userGroup));
        }

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $role->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $role;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($role, array $keys = array())
    {
        $expression = array();

        if (!$role instanceof Role) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'permission',
                'userGroup',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'roles'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $role->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $role->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $role->getDescription();
        }
        if (in_array('permission', $keys)) {
            $attributes['permission'] = $role->getPermission();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $role->getStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => 1
                )
             );
        }

        if (in_array('userGroup', $keys)) {
            $expression['data']['relationships']['userGroup']['data'] = array(
                array(
                    'type' => 'branchOffices',
                    'id' => $role->getUserGroupType()
                )
             );
        }

        return $expression;
    }
}
