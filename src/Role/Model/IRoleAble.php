<?php
namespace Sdk\Role\Model;

interface IRoleAble
{
    const CATEGORY = array(
        'NATURAL_PERSON' => 1100,//用户信息管理模块
        'AUTHENTICATION' => 1200,
        'UN_AUDITED_ENTERPRISE' => 1300,
        'UN_AUDITED_FINANCE_AUTHENTICATION' => 1400,
        'PROFESSION_ALTITLE' => 1500,
        'EDUCATIONAL_AUTHENTICATION' => 1600,//教育机构认证

        'POLICY' => 2111,//内容管理模块
        'POLICY_VIDEO' => 2112,
        'DISPATCH_DEPARTMENT' => 2113,
        'GOVERNMENT_ORGAN' => 2114,
        'CITY_PROMOTION' => 2115,
        'POLICY_SUBJECT' => 2116,
        'POLICY_INTERPRETATION' => 2117,
        'RELATION_PRODUCT' => 2118,

        'PARENT_CATEGORY' => 2211,//服务超市

        'SERVICE_REQUIREMENT' => 2220,
        'SERVICE' => 2230,
        'SERVICE_ORDER' => 2240,
        'EVALUATION' =>2250,//评价管理

        'LOAN_PRODUCT' => 2310,//金融超市
        'APPOINTMENT' => 2320,
        'LOANRE_QUIREMENT' => 2330,

        'FINANCIAL_QA' => 2410,//财智平台
        'MONEY_WISE_NEWS' => 2420,

        //分销货客
        'SYSTEM_TEMPLATE' => 2510,//平台模版
        'TEMPLATE_ELEMENT' => 2520,//元素管理
        'COURSE_REQUIREMENT'=>2620,//课程需求

        'MEMBER' => 4100,//人员管理模块

        'WITHDRAWAL' => 5100,//财务管理模块
        'TRADE_RECORD' => 5200,
        'REFUND_ORDER' => 5300,//退款申请

        'RELEASE_COUPON' => 6100,//营销管理模块

        'LABEL' => 8100,//系统设置模块
        'DICTIONARY' => 8200,
        'LOG' => 8300,
        'BANNER' => 8400,
        'MESSAGE' => 8500,
        'SYSTEM_NOTICE' => 8510,
        'CREDIT_SCORE_GRADE' => 8600,//信用评分等级
        'CREDIT_SCORE_TEMPLATE' => 8700,//信用评分模型
        'QUOTA' => 8800,
        'AREA' => 8900,
        'BRANCH_OFFICE' => 8910,
        'DEPARTMENT' => 8920,

        'TAG' => 9100,//数据标签
        'TAG_STATICS' => 9200,//数据标签统计

        'STRATEGY_CONFIG' => 10000, //全局策略配置
        'USER_TYPE_MANAGE' => 10100, //用户类型管理
        'ENTERPRISE_SORT_BY_ORDER'=>11100,//企业排行
        'ROLE_AUTHORIZATION'=>10300,//角色权限
        'STAFF_MANAGEMENT'=>10400,//员工
    );

    const CATEGORY_CN = array(
        '1000' => '用户信息管理',//用户信息管理模块
        '1100' => '实名认证审核',
        '1200' => '服务商审核',
        '1300' => '企业认证审核',
        '1400' => '金融机构审核',
        '1500' => '职称审核',

        '2000' => '内容管理',//内容管理模块
        '2100' => '汇众主站管理',
        '2110' => '政策管理模块',
        '2111' => '政策管理',
        '2112' => '其他',//预留
        '2113' => '发文部门',
        '2114' => '政府机构',
        '2115' => '城市推广',
        '2116' => '政策专题',

        '2200' => '服务超市',
        '2210' => '服务分类模块',
        '2211' => '服务分类',
        
        '2220' => '需求审核',
        '2230' => '服务审核',
        '2240' => '订单管理',
        '2250' => '评价管理',

        '2300' => '金融超市',
        '2310' => '产品审核',
        '2320' => '申请管理',
        '2330' => '需求管理',
        '2400' => '财智平台',
        '2410' => '问答管理',
        '2420' => '新闻管理',

        '2510' => '平台模版',
        '2520' => '元素管理',
        '2620' => '课程管理',

        '3000' => '城市管理',//城市管理模块

        '4000' => '人员管理',//人员管理模块
        '4100' => '账号管理',

        '5000' => '财务管理',//财务管理模块
        '5100' => '提现申请',
        '5200' => '平台账户管理',
        '5200' => '退款管理',

        '6000' => '营销管理',//营销管理模块
        '6100' => '优惠劵',

        '7000' => '安全管理',//安全管理模块

        '8000' => '系统设置',//系统设置模块
        '8100' => '标签管理',
        '8200' => '字典管理',
        '8300' => '日志管理',
        '8400' => '轮播管理',
        '8500' => '消息管理',
        '8510' => '系统公告',
        '8600' => '信用评分等级',
        '8700' => '信用评分模型',
        '8800' => '指标管理',
        '8900' => '区域标签',
        '8910' => '分公司管理',

        '9100' => '数据标签',
        '9200' => '数据标签统计',

        '10000' => '全局策略',
        '10100' => '用户类型',

        '11100' => '企业排行数据统计',
        '11200' => '课程需求',
        '10300' => '角色管理',
        '10400' => '员工管理',
    );

    const OPERATION = array(
        'OPERATION_SHOW' => '1',
        'OPERATION_ADD' => '2',
        'OPERATION_EDIT' => '3',
        'OPERATION_SHELF' => '4',
        'OPERATION_ENABLE' => '5',
        'OPERATION_REJECT' => '6',
        'OPERATION_TOP' => '7',
        'OPERATION_DELETE' => '8',
        'OPERATION_RESUBMIT' => '9',
        'OPERATION_REVOKE' => '10',
        'OPERATION_POLICY_VIDEO' => '11',
        'OPERATION_POLICY_INTERPRETATION' => '12',
        'OPERATION_RELATION_PRODUCT' => '13',
        'OPERATION_RELATION_SPECIAL' => '14',
        'OPERATION_MASK' => '15',
        'OPERATION_RESELECTION' => '16',
        'OPERATION_POLICY_VOIDE' => '17',
        'OPERATION_RECOMMEND' => '18',
        'OPERATION_TRANSFER_COMPLETED' => '19',
        'OPERATION_SET_APPLICATION_SCENE' => '20',
        'OPERATION_PAYMENT_REFUND_ORDER' => '21',
        'OPERATION_AUTHENTICATION_RELATION' => '22',
        'OPERATION_SET_DEFAULT' => '23',
        'OPERATION_DOWNLOAD' => '24',
        'RESET_PASSWORD' => '25',
        'ASSIGN_ROLES' => '26',
        'AFFILIATED_BRANCH' => '27',
        'RELATED_DEPARTMENT' => '28',
        'ASSIG_PERMISSIONS' => '29',
    );

    const OPERATION_CN = array(
        '1' => '查看',
        '2' => '添加',
        '3' => '编辑',
        '4' => '上架/下架',
        '5' => '启用/禁用',
        '6' => '审核通过/驳回',
        '7' => '置顶/取消置顶',
        '8' => '删除',
        '9' => '重新提交',
        '10' => '撤销',
        '11' => '政策视频',
        '12' => '发布政策解读',
        '13' => '关联产品/取消关联',
        '14' => '推荐至专题',
        '15' => '屏蔽/取消屏蔽',
        '16' => '重新选择',
        '17' => '发布视频',
        '18' => '推荐/取消推荐',
        '19' => '转账',
        '20' => '设置应用场景',
        '21' => '确认打款',
        '22' => '关联用户类型/取消关联',
        '23' => '设置/取消默认',
        '24' => '导出',
        '25' => '重置密码',
        '26' => '分配角色',
        '27' => '关联分公司/取消关联',
        '28' => '关联部门/取消关联',
        '29' => '分配权限',
    );

    public function getRole() : Role;
}
