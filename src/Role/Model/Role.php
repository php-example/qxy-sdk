<?php
namespace Sdk\Role\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Crew\Model\Crew;
use Sdk\BranchOffice\Model\BranchOffice;

use Sdk\Role\Repository\RoleRepository;

class Role implements IObject, IOperatAble, IEnableAble
{
    use Object, OperatAbleTrait, EnableAbleTrait;

    private $id;

    private $name;

    private $description;

    private $userGroupType;

    private $permission;

    private $crew;

    private $branchOffice;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->description = '';
        $this->userGroupType = '';
        $this->permission = array();
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->branchOffice = new BranchOffice();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new RoleRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->description);
        unset($this->userGroupType);
        unset($this->branchOffice);
        unset($this->permission);
        unset($this->crew);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setUserGroupType(string $userGroupType) : void
    {
        $this->userGroupType = $userGroupType;
    }

    public function getUserGroupType() : string
    {
        return $this->userGroupType;
    }

    public function setBranchOffice(BranchOffice $branchOffice) : void
    {
        $this->branchOffice = $branchOffice;
    }

    public function getBranchOffice() : BranchOffice
    {
        return $this->branchOffice;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setPermission(array $permission) : void
    {
        $this->permission = $permission;
    }

    public function getPermission() : array
    {
        return $this->permission;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : RoleRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    public function permission() : bool
    {
        return $this->getRepository()->permission($this);
    }
}
