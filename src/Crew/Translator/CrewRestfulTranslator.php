<?php
namespace Sdk\Crew\Translator;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;
use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\User\Translator\UserRestfulTranslator;

class CrewRestfulTranslator extends UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $crew = null)
    {
        return $this->translateToObject($expression, $crew);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return NullCrew::getInstance();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $crew = parent::translateToObject($expression, $crew);

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $crew->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['workNumber'])) {
            $crew->setWorkNumber($attributes['workNumber']);
        }
        if (isset($attributes['isAdmin'])) {
            $crew->setIsAdmin($attributes['isAdmin']);
        }
        if (isset($attributes['realName'])) {
            $crew->getRealName($attributes['realName']);
        }
        if (isset($attributes['cellphone'])) {
            $crew->getCellphone($attributes['cellphone']);
        }
        if (isset($attributes['remark'])) {
            $crew->setRemark($attributes['remark']);
        }
        if (isset($attributes['loginTime'])) {
            $crew->setSignInTime($attributes['loginTime']);
        }
        if (isset($attributes['roles'])) {
            $crew->setRoles($attributes['roles']);
        }
        if (isset($attributes['permission'])) {
            $crew->getPermission($attributes['permission']);
        }
        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        $user = parent::objectToArray($crew, $keys);

        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'workNumber',
                'remark',
                'status',
                'roles',
                'userGroup',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'crews',
                'id'=>$crew->getId()
            )
        );

        $attributes = array();

        if (in_array('workNumber', $keys)) {
            $attributes['workNumber'] = $crew->getWorkNumber();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $crew->getStatus();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $crew->getRemark();
        }
        if (in_array('roles', $keys)) {
            $attributes['roles'] = $crew->getRoles();
        }

        $expression['data']['attributes'] = array_merge($user['data']['attributes'], $attributes);

        if (in_array('userGroup', $keys)) {
            $expression['data']['relationships']['userGroup']['data'] = array(
                array(
                    'type' => 'branchOffices',
                    'id' => $crew->getUserGroup()
                )
             );
        }

        return $expression;
    }
}
