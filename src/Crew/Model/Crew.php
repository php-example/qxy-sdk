<?php
namespace Sdk\Crew\Model;

use Sdk\User\Model\User;

use Sdk\Crew\Repository\CrewRepository;

class Crew extends User
{
    /**
     * [$workNumber 工号]
     * @var [string]
     */
    private $workNumber;
    /**
     * [$repository]
     * @var [Object]
     */
    private $repository;
    /**
     * [$remark 备注]
     * @var [string]
     */
    private $remark;
    /**
     * [$signInTime 最近登陆时间]
     * @var [string]
     */
    private $signInTime;

    /**
     * [$roles 角色]
     * @var [string]
     */
    private $roles;

    private $isAdmin;

    private $userGroup;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->workNumber = '';
        $this->isAdmin = 0;
        $this->repository = new CrewRepository();
        $this->remark = '';
        $this->signInTime = '';
        $this->roles = '';
        $this->userGroup = '';
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->workNumber);
        unset($this->isAdmin);
        unset($this->repository);
        unset($this->remark);
        unset($this->signInTime);
        unset($this->roles);
        unset($this->permission);
        unset($this->userGroup);
    }

    public function setWorkNumber(string $workNumber) : void
    {
        $this->workNumber = $workNumber;
    }

    public function getWorkNumber() : string
    {
        return $this->workNumber;
    }

    public function setIsAdmin(int $isAdmin) : void
    {
        $this->isAdmin = $isAdmin;
    }

    public function getIsAdmin() : int
    {
        return $this->isAdmin;
    }

    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setSignInTime(string $signInTime) : void
    {
        $this->signInTime = $signInTime;
    }

    public function getSignInTime() : string
    {
        return $this->signInTime;
    }

    public function setRoles(string $roles) : void
    {
        $this->roles = $roles;
    }

    public function getRoles() : string
    {
        return $this->roles;
    }

    public function setUserGroup(string $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : string
    {
        return $this->userGroup;
    }

    /**
     * 初始化密码
     * @return [bool]
     */

    public function passwordInit() : bool
    {
        return $this->getRepository()->passwordInit($this);
    }

    /**
     * 分配角色
     * @return [bool]
     */
    public function distributionRole(): bool
    {
        return $this->getRepository()->distributionRole($this);
    }

    public function cancelRelationRole() : bool
    {
        $repository = $this->getRepository();

        return $repository->cancelRelationRole($this);
    }
}
