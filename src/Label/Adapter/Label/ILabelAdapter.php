<?php
namespace Sdk\Label\Adapter\Label;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface ILabelAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
