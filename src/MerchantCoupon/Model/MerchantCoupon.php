<?php
namespace Sdk\MerchantCoupon\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Enterprise\Model\NullEnterprise;

use Sdk\MerchantCoupon\Repository\MerchantCouponRepository;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class MerchantCoupon implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const RELEASE_TYPE = array(
        'PLATFORM' => 0,  //平台
        'MERCHANT' => 2,  //商家
    );

    const USE_SCENARIO = array(
        'NORMAL' => 0,  //无限制
        'REGISTER' => 1, //注册送优惠券
        'ORDER_CONFIRMATION' => 2, //确认订单送优惠券
    );

    const STATUS = array(
        'NORMAL' => 0,  //正常
        'EXPIRED' => -2, //已过期
        'DISCONTINUE' => -4, //已停用
        'DELETED' => -6  //已删除
    );

    const COUPON_TYPE = array(
        'FULL_REDUCTION' => 0,  //满减券
        'DISCOUNT' => 2  //折扣券
    );

    const RECEIVING_MODE = array(
        'MANUAL' => 0,  //手动领取
        'AUTOMATIC' => 2  //平台自动发放
    );

    const RECEIVING_USERS = array(
        'ALL' => 0,  //全部用户
        'NEW' => 2,  //新人用户
        'VIP' => 4  //vip用户
    );

    const APPLY_SCOPE = array(
        'PLATFORM_CURRENCY' => 1,  //平台通用
        'STORE_CURRENCY' => 2,  //店铺通用
        'DESIGNATE_CATEGORY' => 3,  //指定分类
        'DESIGNATE_DELETED' => 4,  //指定商品
        'DESIGNATE_STORE' => 5,  //指定店铺
    );

    const IS_SUPERPOSITION = array(
        'NO' => 0,  //优惠券是否和店铺优惠券叠加使用
        'YES' => 2,
    );

    /**
     * @var int $id 主键id
     */
    private $id;
    /**
     * @var int $couponType 优惠券类型
     */
    private $couponType;
    /**
     * @var float $denomination 优惠面额
     */
    private $denomination;
    /**
     * @var string $name 优惠劵名称
     */
    private $name;
    /**
     * @var int $discount 优惠券折扣值
     */
    private $discount;
    /**
     * @var int $useStandard 优惠券使用门槛
     */
    private $useStandard;
    /**
     * @var string $validityStartTime 优惠券开始使用时间
     */
    private $validityStartTime;
    /**
     * @var string $validityEndTime 优惠券结束使用时间
     */
    private $validityEndTime;
    /**
     * @var int $issueTotal 优惠券总发行量
     */
    private $issueTotal;
    /**
     * @var int $distributeTotal 优惠券总领取数量
     */
    private $distributeTotal;
    /**
     * @var int $perQuota 优惠券每人限领
     */
    private $perQuota;
    /**
     * @var int $useScenario 优惠券领取场景
     */
    private $useScenario;
    /**
     * @var int $receivingMode 优惠券领取方式
     */
    private $receivingMode;
    /**
     * @var int $receivingUsers 优惠券可领取用户
     */
    private $receivingUsers;
    /**
     * @var int $isSuperposition 优惠券是否和店铺优惠券叠加使用
     */
    private $isSuperposition;
    /**
     * @var int $applyScope 优惠券使用范围
     */
    private $applyScope;
    /**
     * @var array $applySituation 优惠券适用情况
     */
    private $applySituation;
    /**
     * @var string $number 优惠券编号
     */
    private $number;
    /**
     * @var float $receiveRate 优惠券领取率
     */
    private $receiveRate;
    /**
     * @var float $receiveRate 优惠券使用率
     */
    private $useRate;
    /**
     * @var float $costEffectivenessRatio 优惠券费效比
     */
    private $costEffectivenessRatio;
    /**
     * @var float $costEffectivenessRatio 优惠券费效比
     */
    private $pullingRate;
    /**
     * @var float $releaseType 判断当前发布优惠券的是平台还是店铺
     */
    private $releaseType;

    private $reference;

    private $repository;


    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->releaseType = self::RELEASE_TYPE['PLATFORM'];
        $this->reference = new NullEnterprise();
        $this->name = '';
        $this->couponType = self::COUPON_TYPE['FULL_REDUCTION'];
        $this->denomination = 0;
        $this->discount = 0;
        $this->useStandard = 0;
        $this->validityStartTime = '';
        $this->validityEndTime = '';
        $this->issueTotal = 0;
        $this->distributeTotal = 0;
        $this->perQuota = 0;
        $this->useScenario = self::USE_SCENARIO['NORMAL'];
        $this->receivingMode = self::RECEIVING_MODE['MANUAL'];
        $this->receivingUsers = self::RECEIVING_USERS['ALL'];
        $this->isSuperposition = self::IS_SUPERPOSITION['NO'];
        $this->applyScope = self::APPLY_SCOPE['PLATFORM_CURRENCY'];
        $this->applySituation = array();
        $this->number = '';
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new MerchantCouponRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->releaseType);
        unset($this->reference);
        unset($this->name);
        unset($this->couponType);
        unset($this->denomination);
        unset($this->discount);
        unset($this->useStandard);
        unset($this->validityStartTime);
        unset($this->validityEndTime);
        unset($this->issueTotal);
        unset($this->distributeTotal);
        unset($this->perQuota);
        unset($this->useScenario);
        unset($this->receivingMode);
        unset($this->receivingUsers);
        unset($this->isSuperposition);
        unset($this->applyScope);
        unset($this->applySituation);
        unset($this->number);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setReleaseType(int $releaseType) : void
    {
        $this->releaseType = $releaseType;
    }

    public function getReleaseType() : int
    {
        return $this->releaseType;
    }

    public function setReference(IMerchantCouponAble $reference)
    {
        $this->reference = $reference;
    }

    public function getReference() : IMerchantCouponAble
    {
        return $this->reference;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setCouponType(int $couponType) : void
    {
        $this->couponType = $couponType;
    }

    public function getCouponType() : int
    {
        return $this->couponType;
    }

    public function setDenomination(float $denomination) : void
    {
        $this->denomination = $denomination;
    }

    public function getDenomination() : float
    {
        return $this->denomination;
    }

    public function setDiscount(int $discount) : void
    {
        $this->discount = $discount;
    }

    public function getDiscount() : int
    {
        return $this->discount;
    }

    public function setUseStandard(float $useStandard) : void
    {
        $this->useStandard = $useStandard;
    }

    public function getUseStandard() : float
    {
        return $this->useStandard;
    }

    public function setValidityStartTime(string $validityStartTime) : void
    {
        $this->validityStartTime = $validityStartTime;
    }

    public function getValidityStartTime() : string
    {
        return $this->validityStartTime;
    }

    public function setValidityEndTime(string $validityEndTime) : void
    {
        $this->validityEndTime = $validityEndTime;
    }

    public function getValidityEndTime() : string
    {
        return $this->validityEndTime;
    }

    public function setIssueTotal(int $issueTotal) : void
    {
        $this->issueTotal = $issueTotal;
    }

    public function getIssueTotal() : int
    {
        return $this->issueTotal;
    }

    public function setDistributeTotal(int $distributeTotal) : void
    {
        $this->distributeTotal = $distributeTotal;
    }

    public function getDistributeTotal() : int
    {
        return $this->distributeTotal;
    }

    public function setPerQuota(int $perQuota) : void
    {
        $this->perQuota = $perQuota;
    }

    public function getPerQuota() : int
    {
        return $this->perQuota;
    }

    public function setReceivingMode(int $receivingMode) : void
    {
        $this->receivingMode = $receivingMode;
    }

    public function getReceivingMode() : int
    {
        return $this->receivingMode;
    }

    public function setUseScenario(int $useScenario) : void
    {
        $this->useScenario = $useScenario;
    }

    public function getUseScenario() : int
    {
        return $this->useScenario;
    }

    public function setReceivingUsers(int $receivingUsers) : void
    {
        $this->receivingUsers = $receivingUsers;
    }

    public function getReceivingUsers() : int
    {
        return $this->receivingUsers;
    }

    public function setIsSuperposition(int $isSuperposition) : void
    {
        $this->isSuperposition = $isSuperposition;
    }

    public function getIsSuperposition() : int
    {
        return $this->isSuperposition;
    }

    public function setApplyScope(int $applyScope) : void
    {
        $this->applyScope = $applyScope;
    }

    public function getApplyScope() : int
    {
        return $this->applyScope;
    }

    public function setApplySituation(array $applySituation) : void
    {
        $this->applySituation = $applySituation;
    }

    public function getApplySituation() : array
    {
        return $this->applySituation;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getReceiveRate() : float
    {
        return $this->receiveRate;
    }

    public function getUseRate() : float
    {
        return $this->useRate;
    }

    public function getCostEffectivenessRatio() : float
    {
        return $this->costEffectivenessRatio;
    }

    public function getPullingRate() : float
    {
        return $this->pullingRate;
    }

    protected function getRepository() : MerchantCouponRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function discontinue() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getRepository()->discontinue($this);
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }

    protected function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    protected function isExpired() : bool
    {
        return $this->getStatus() == self::STATUS['EXPIRED'];
    }

    protected function isDeleted() : bool
    {
        return $this->getStatus() == self::STATUS['DELETED'];
    }
}
