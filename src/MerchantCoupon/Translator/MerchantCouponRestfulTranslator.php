<?php
namespace Sdk\MerchantCoupon\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\MerchantCoupon\Model\NullMerchantCoupon;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class MerchantCouponRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    const TYPE_MAPS = array(
        MerchantCoupon::RELEASE_TYPE['PLATFORM'] => 'crews',
        MerchantCoupon::RELEASE_TYPE['MERCHANT'] => 'enterprises'
    );

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $merchantCoupon = null)
    {
        return $this->translateToObject($expression, $merchantCoupon);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $merchantCoupon = null)
    {
        if (empty($expression)) {
            return NullMerchantCoupon::getInstance();
        }

        if ($merchantCoupon == null) {
            $merchantCoupon = new MerchantCoupon();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $merchantCoupon->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['releaseType'])) {
            $merchantCoupon->setReleaseType($attributes['releaseType']);
        }
        if (isset($attributes['name'])) {
            $merchantCoupon->setName($attributes['name']);
        }
        if (isset($attributes['couponType'])) {
            $merchantCoupon->setCouponType($attributes['couponType']);
        }
        if (isset($attributes['denomination'])) {
            $merchantCoupon->setDenomination($attributes['denomination']);
        }
        if (isset($attributes['discount'])) {
            $merchantCoupon->setDiscount($attributes['discount']);
        }
        if (isset($attributes['useStandard'])) {
            $merchantCoupon->setUseStandard($attributes['useStandard']);
        }
        if (isset($attributes['validityStartTime'])) {
            $merchantCoupon->setValidityStartTime($attributes['validityStartTime']);
        }
        if (isset($attributes['validityEndTime'])) {
            $merchantCoupon->setValidityEndTime($attributes['validityEndTime']);
        }
        if (isset($attributes['issueTotal'])) {
            $merchantCoupon->setIssueTotal($attributes['issueTotal']);
        }
        if (isset($attributes['distributeTotal'])) {
            $merchantCoupon->setDistributeTotal($attributes['distributeTotal']);
        }
        if (isset($attributes['perQuota'])) {
            $merchantCoupon->setPerQuota($attributes['perQuota']);
        }
        if (isset($attributes['useScenario'])) {
            $merchantCoupon->setUseScenario($attributes['useScenario']);
        }
        if (isset($attributes['receivingMode'])) {
            $merchantCoupon->setReceivingMode($attributes['receivingMode']);
        }
        if (isset($attributes['receivingUsers'])) {
            $merchantCoupon->setReceivingUsers($attributes['receivingUsers']);
        }
        if (isset($attributes['isSuperposition'])) {
            $merchantCoupon->setIsSuperposition($attributes['isSuperposition']);
        }
        if (isset($attributes['applyScope'])) {
            $merchantCoupon->setApplyScope($attributes['applyScope']);
        }
        if (isset($attributes['applySituation'])) {
            $merchantCoupon->setApplySituation($attributes['applySituation']);
        }
        if (isset($attributes['number'])) {
            $merchantCoupon->setNumber($attributes['number']);
        }
        if (isset($attributes['status'])) {
            $merchantCoupon->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $merchantCoupon->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $merchantCoupon->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $merchantCoupon->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['reference']['data'])) {
            if (isset($expression['included'])) {
                $reference = $this->changeArrayFormat($relationships['reference']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $reference = $this->changeArrayFormat($relationships['reference']['data']);
            }

            $referenceRestfulTranslator = $this->getTranslatorFactory()->getTranslator($attributes['releaseType']);
            $merchantCoupon->setReference($referenceRestfulTranslator->arrayToObject($reference));
        }

        return $merchantCoupon;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($merchantCoupon, array $keys = array())
    {
        $expression = array();

        if (!$merchantCoupon instanceof MerchantCoupon) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'name',
                'couponType',
                'denomination',
                'discount',
                'useStandard',
                'validityStartTime',
                'validityEndTime',
                'issueTotal',
                'distributeTotal',
                'perQuota',
                'useScenario',
                'receivingMode',
                'receivingUsers',
                'isSuperposition',
                'applyScope',
                'applySituation',
                'status',
                'createTime',
                'updateTime',
                'releaseType',
                'reference'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'releaseCoupons'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $merchantCoupon->getId();
        }

        $attributes = array();

        if (in_array('number', $keys)) {
            $attributes['number'] = $merchantCoupon->getNumber();
        }
        if (in_array('name', $keys)) {
            $attributes['name'] = $merchantCoupon->getName();
        }
        if (in_array('couponType', $keys)) {
            $attributes['couponType'] = $merchantCoupon->getCouponType();
        }
        if (in_array('denomination', $keys)) {
            $attributes['denomination'] = $merchantCoupon->getDenomination();
        }
        if (in_array('discount', $keys)) {
            $attributes['discount'] = $merchantCoupon->getDiscount();
        }
        if (in_array('useStandard', $keys)) {
            $attributes['useStandard'] = $merchantCoupon->getUseStandard();
        }
        if (in_array('validityStartTime', $keys)) {
            $attributes['validityStartTime'] = $merchantCoupon->getValidityStartTime();
        }
        if (in_array('validityEndTime', $keys)) {
            $attributes['validityEndTime'] = $merchantCoupon->getValidityEndTime();
        }
        if (in_array('issueTotal', $keys)) {
            $attributes['issueTotal'] = $merchantCoupon->getIssueTotal();
        }
        if (in_array('distributeTotal', $keys)) {
            $attributes['distributeTotal'] = $merchantCoupon->getDistributeTotal();
        }
        if (in_array('perQuota', $keys)) {
            $attributes['perQuota'] = $merchantCoupon->getPerQuota();
        }
        if (in_array('useScenario', $keys)) {
            $attributes['useScenario'] = $merchantCoupon->getUseScenario();
        }
        if (in_array('receivingMode', $keys)) {
            $attributes['receivingMode'] = $merchantCoupon->getReceivingMode();
        }
        if (in_array('receivingUsers', $keys)) {
            $attributes['receivingUsers'] = $merchantCoupon->getReceivingUsers();
        }
        if (in_array('isSuperposition', $keys)) {
            $attributes['isSuperposition'] = $merchantCoupon->getIsSuperposition();
        }
        if (in_array('applyScope', $keys)) {
            $attributes['applyScope'] = $merchantCoupon->getApplyScope();
        }
        if (in_array('applySituation', $keys)) {
            $attributes['applySituation'] = $merchantCoupon->getApplySituation();
        }
        if (in_array('number', $keys)) {
            $attributes['number'] = $merchantCoupon->getNumber();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $merchantCoupon->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $attributes['statusTime'] = $merchantCoupon->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $attributes['createTime'] = $merchantCoupon->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $attributes['updateTime'] = $merchantCoupon->getUpdateTime();
        }
        if (in_array('releaseType', $keys)) {
            $attributes['releaseType'] = $merchantCoupon->getReleaseType();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('reference', $keys)) {
            $releaseType = $attributes['releaseType'];
            $type = self::TYPE_MAPS[$releaseType];

            $expression['data']['relationships']['reference']['data'] = array(
                array(
                    'type' => $type,
                    'id' => $merchantCoupon->getReference()->getId()
                )
            );
        }

        return $expression;
    }
}
