<?php
namespace Sdk\MerchantCoupon\Adapter\MerchantCoupon;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\MerchantCoupon\Utils\MockFactory;

use Sdk\Common\Adapter\OperatAbleMockAdapterTrait;

class MerchantCouponMockAdapter implements IMerchantCouponAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateMerchantCouponObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $merchantCouponList = array();

        foreach ($ids as $id) {
            $merchantCouponList[] = MockFactory::generateMerchantCouponObject($id);
        }

        return $merchantCouponList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateMerchantCouponObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $merchantCouponList = array();

        foreach ($ids as $id) {
            $merchantCouponList[] = MockFactory::generateMerchantCouponObject($id);
        }

        return $merchantCouponList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function deletes(MerchantCoupon $merchantCoupon) : bool
    {
        unset($merchantCoupon);

        return true;
    }

    public function discontinue(MerchantCoupon $merchantCoupon) : bool
    {
        unset($merchantCoupon);

        return true;
    }
}
