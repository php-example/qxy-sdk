<?php
namespace Sdk\MerchantCoupon\Adapter\MerchantCoupon;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\MerchantCoupon\Model\MerchantCoupon;

interface IMerchantCouponAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
    public function deletes(MerchantCoupon $merchantCoupon) : bool;

    public function discontinue(MerchantCoupon $merchantCoupon) : bool;
}
