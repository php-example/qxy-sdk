<?php
namespace Sdk\MerchantCoupon\Adapter\MerchantCoupon;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Sdk\MerchantCoupon\Model\MerchantCoupon;
use Sdk\MerchantCoupon\Model\NullMerchantCoupon;
use Sdk\MerchantCoupon\Translator\MerchantCouponRestfulTranslator;

class MerchantCouponRestfulAdapter extends GuzzleAdapter implements IMerchantCouponAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'PORTAL_COUPON_LIST' => [
            'fields' =>[],
            'include' => 'reference'
        ],
        'OA_COUPON_LIST' => [
            'fields' =>[],
            'include' => 'reference'
        ],
        'COUPON_FETCH_ONE'=>[
            'fields' => [],
            'include' => 'reference'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new MerchantCouponRestfulTranslator();
        $this->resource = 'releaseCoupons';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            203 => COUPON_NAME_FORMAT_ERROR
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullMerchantCoupon::getInstance());
    }

    protected function addAction(MerchantCoupon $merchantCoupon): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $merchantCoupon,
            array(
                'name',
                'couponType',
                'denomination',
                'discount',
                'useStandard',
                'validityStartTime',
                'validityEndTime',
                'issueTotal',
                'distributeTotal',
                'perQuota',
                'useScenario',
                'receivingMode',
                'receivingUsers',
                'isSuperposition',
                'applyScope',
                'applySituation',
                'releaseType',
                'reference'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($merchantCoupon);
            return true;
        }

        return false;
    }

    public function discontinue(MerchantCoupon $merchantCoupon) : bool
    {
        $this->patch(
            $this->getResource().'/'.$merchantCoupon->getId().'/discontinue'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($merchantCoupon);
            return true;
        }

        return false;
    }

    public function deletes(MerchantCoupon $merchantCoupon) : bool
    {
        $this->patch(
            $this->getResource().'/'.$merchantCoupon->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($merchantCoupon);
            return true;
        }

        return false;
    }

    protected function editAction(MerchantCoupon $merchantCoupon): bool
    {
        unset($merchantCoupon);
        return false;
    }
}
