<?php
namespace Sdk\MerchantCoupon\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

use Sdk\MerchantCoupon\Adapter\MerchantCoupon\IMerchantCouponAdapter;
use Sdk\MerchantCoupon\Adapter\MerchantCoupon\MerchantCouponMockAdapter;
use Sdk\MerchantCoupon\Adapter\MerchantCoupon\MerchantCouponRestfulAdapter;

use Sdk\MerchantCoupon\Model\MerchantCoupon;

class MerchantCouponRepository extends Repository implements IMerchantCouponAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_COUPON_LIST';
    const OA_LIST_MODEL_UN = 'OA_COUPON_LIST';
    const FETCH_ONE_MODEL_UN = 'COUPON_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new MerchantCouponRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IMerchantCouponAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IMerchantCouponAdapter
    {
        return new MerchantCouponMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(MerchantCoupon $merchantCoupon) : bool
    {
        return $this->getAdapter()->deletes($merchantCoupon);
    }

    public function discontinue(MerchantCoupon $merchantCoupon) : bool
    {
        return $this->getAdapter()->discontinue($merchantCoupon);
    }
}
