<?php
namespace Sdk\Member\Translator;

use Sdk\Member\Model\Member;
use Sdk\Member\Model\NullMember;

use Utils\Utils\Mask;

use Marmot\Interfaces\ITranslator;

class MemberTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $member = null)
    {
        unset($member);
        unset($expression);
        return NullMember::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($member, array $keys = array())
    {
        if (!$member instanceof Member) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'avatar',
                'nickName',
                'gender',
                'birthday',
                'area',
                'address',
                'briefIntroduction',
                'cellphone',
                'realName',
                'userName',
                'createTime',
                'updateTime',
                'pushIdentity',
                'tag',
                'equipmentType'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($member->getId());
        }
        if (in_array('avatar', $keys)) {
            $expression['avatar'] = $member->getAvatar();
        }
        if (in_array('nickName', $keys)) {
            $expression['nickName'] = $member->getNickName();
        }
        if (in_array('gender', $keys)) {
            $expression['gender'] = $member->getGender();
        }
        if (in_array('birthday', $keys)) {
            $expression['birthday'] = $member->getBirthday();
        }
        if (in_array('area', $keys)) {
            $expression['area'] = $member->getArea();
        }
        if (in_array('address', $keys)) {
            $expression['address'] = $member->getAddress();
        }
        if (in_array('briefIntroduction', $keys)) {
            $expression['briefIntroduction'] = $member->getBriefIntroduction();
        }
        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $member->getCellphone();
            $expression['cellphoneMask'] = Mask::mask($expression['cellphone'], 3, 4);
        }
        if (in_array('realName', $keys)) {
            $expression['realName'] = $member->getRealName();
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $member->getUserName();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $member->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d', $member->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $member->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d', $member->getUpdateTime());
        }
        if (in_array('pushIdentity', $keys)) {
            $expression['pushIdentity'] = $member->getPushIdentity();
        }
        if (in_array('tag', $keys)) {
            $expression['tag'] = $member->getTag();
        }
        if (in_array('equipmentType', $keys)) {
            $expression['equipmentType'] = $member->getEquipmentType();
        }

        return $expression;
    }
}
