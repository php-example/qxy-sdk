<?php
namespace Sdk\Member\Translator;

use Sdk\Member\Model\Member;
use Sdk\Member\Model\NullMember;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\User\Translator\UserRestfulTranslator;

class MemberRestfulTranslator extends UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $member = null)
    {
        return $this->translateToObject($expression, $member);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $member = null)
    {
        if (empty($expression)) {
            return NullMember::getInstance();
        }

        if ($member == null) {
            $member = new Member();
        }

        $member = parent::translateToObject($expression, $member);

        $data =  $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['nickName'])) {
            $member->setNickName($attributes['nickName']);
        }
        if (isset($attributes['birthday'])) {
            $member->setBirthday($attributes['birthday']);
        }
        if (isset($attributes['area'])) {
            $member->setArea($attributes['area']);
        }
        if (isset($attributes['address'])) {
            $member->setAddress($attributes['address']);
        }
        if (isset($attributes['briefIntroduction'])) {
            $member->setBriefIntroduction($attributes['briefIntroduction']);
        }
        if (isset($attributes['enterpriseStatus'])) {
            $member->setEnterpriseStatus($attributes['enterpriseStatus']);
        }
        if (isset($attributes['naturalPersonStatus'])) {
            $member->setNaturalPersonStatus($attributes['naturalPersonStatus']);
        }
        if (isset($attributes['encryptPassword'])) {
            $member->setEncryptPassword($attributes['encryptPassword']);
        }
        if (isset($attributes['pushIdentity'])) {
            $member->setPushIdentity($attributes['pushIdentity']);
        }
        if (isset($attributes['tag'])) {
            $member->setTag($attributes['tag']);
        }
        if (isset($attributes['binaries'])) {
            $member->setBinaries($attributes['binaries']);
        }
        if (isset($attributes['equipmentType'])) {
            $member->setEquipmentType($attributes['equipmentType']);
        }

        return $member;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($member, array $keys = array())
    {
        $user = parent::objectToArray($member, $keys);

        if (!$member instanceof Member) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'nickName',
                'birthday',
                'area',
                'address',
                'briefIntroduction',
                'pushIdentity',
                'tag',
                'equipmentType'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'members',
                'id'=>$member->getId()
            )
        );

        $attributes = array();

        if (in_array('nickName', $keys)) {
            $attributes['nickName'] = $member->getNickName();
        }
        if (in_array('birthday', $keys)) {
            $attributes['birthday'] = $member->getBirthday();
        }
        if (in_array('area', $keys)) {
            $attributes['area'] = $member->getArea();
        }
        if (in_array('address', $keys)) {
            $attributes['address'] = $member->getAddress();
        }
        if (in_array('briefIntroduction', $keys)) {
            $attributes['briefIntroduction'] = $member->getBriefIntroduction();
        }
        if (in_array('pushIdentity', $keys)) {
            $attributes['pushIdentity'] = $member->getPushIdentity();
        }
        if (in_array('tag', $keys)) {
            $attributes['tag'] = $member->getTag();
        }
        if (in_array('equipmentType', $keys)) {
            $attributes['equipmentType'] = $member->getEquipmentType();
        }

        $expression['data']['attributes'] = array_merge($user['data']['attributes'], $attributes);

        return $expression;
    }
}
