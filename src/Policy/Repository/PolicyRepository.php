<?php
namespace Sdk\Policy\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\OnShelfAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;

use Sdk\Policy\Adapter\Policy\IPolicyAdapter;
use Sdk\Policy\Adapter\Policy\PolicyMockAdapter;
use Sdk\Policy\Adapter\Policy\PolicyRestfulAdapter;

use Sdk\Policy\Model\Policy;
use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class PolicyRepository extends Repository implements IPolicyAdapter
{
    use FetchRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_POLICY_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_POLICY_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'POLICY_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PolicyRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPolicyAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPolicyAdapter
    {
        return new PolicyMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function relationProduct(Policy $policy) : bool
    {
        return $this->getAdapter()->relationProduct($policy);
    }

    public function cancelRelationProduct(Policy $policy) : bool
    {
        return $this->getAdapter()->cancelRelationProduct($policy);
    }

    public function relationSpecial(Policy $policy) : bool
    {
        return $this->getAdapter()->relationSpecial($policy);
    }

    public function tagSearchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) {
        return $this->getAdapter()->tagSearchAsync($filter, $sort, $offset, $size);
    }

    public function portalFetchOne($id,$type)
    {
        return $this->getAdapter()->portalFetchOne($id,$type);
    }

    public function addShareRecord(Policy $policy,int $type) : bool
    {
        return $this->getAdapter()->addShareRecord($policy, $type);
    }
}
