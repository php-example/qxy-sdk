<?php
namespace Sdk\Policy\Repository;

use Sdk\PolicyProduct\Model\PolicyProduct;

use Common\Repository\NullRepository;

class RepositoryFactory
{
    const MAPS = array(
        PolicyProduct::CATEGORY['SERVICE']=>
        'Sdk\Service\Repository\ServiceRepository',
        PolicyProduct::CATEGORY['FINANCE']=>
        'Sdk\LoanProduct\Repository\LoanProductRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}
