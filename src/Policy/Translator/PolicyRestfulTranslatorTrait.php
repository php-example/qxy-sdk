<?php
namespace Sdk\Policy\Translator;

use Sdk\Policy\Model\Policy;

use Sdk\Policy\Model\PolicyModelFactory;

trait PolicyRestfulTranslatorTrait
{
    protected function setUpApplicableObjects(array $applicableObjects, Policy $policy)
    {
        foreach ($applicableObjects as $applicableObjectsArray) {
            $applicableObjects = $this->changeArrayFormat($applicableObjectsArray);
            $applicableObjectsObject = $this->getDictionaryRestfulTranslator()
                                            ->arrayToObject($applicableObjects);
            $policy->addApplicableObject($applicableObjectsObject);
        }
    }

    protected function setUpApplicableIndustries(array $applicableIndustries, Policy $policy)
    {
        foreach ($applicableIndustries as $applicableIndustriesArray) {
            $applicableIndustries = $this->changeArrayFormat($applicableIndustriesArray);
            $applicableIndustriesObject = $this->getDictionaryRestfulTranslator()
                                            ->arrayToObject($applicableIndustries);
            $policy->addApplicableIndustry($applicableIndustriesObject);
        }
    }

    protected function setUpClassifies(array $classifies, Policy $policy)
    {
        foreach ($classifies as $classifiesArray) {
            $classifies = $this->changeArrayFormat($classifiesArray);
            $classifiesObject = $this->getDictionaryRestfulTranslator()
                                            ->arrayToObject($classifies);
            $policy->addClassify($classifiesObject);
        }
    }

    protected function setUpDispatchDepartments(array $dispatchDepartments, Policy $policy)
    {
        foreach ($dispatchDepartments as $dispatchDepartmentsArray) {
            $dispatchDepartments = $this->changeArrayFormat($dispatchDepartmentsArray);
            $dispatchDepartmentsObject = $this->getDispatchDepartmentRestfulTranslator()
                                            ->arrayToObject($dispatchDepartments);
            $policy->addDispatchDepartment($dispatchDepartmentsObject);
        }
    }

    protected function setUpLabels(array $labels, Policy $policy)
    {
        foreach ($labels as $labelsArray) {
            $labels = $this->changeArrayFormat($labelsArray);
            $labelsObject = $this->getLabelRestfulTranslator()->arrayToObject($labels);
            $policy->addLabel($labelsObject);
        }
    }
    
    protected function setUpSpecials(array $specials, Policy $policy)
    {
        foreach ($specials as $specialsArray) {
            $specials = $this->changeArrayFormat($specialsArray);
            $specialsObject = $this->getPolicySubjectRestfulTranslator()->arrayToObject($specials);
            $policy->addSpecial($specialsObject);
        }
    }
    
    protected function setUpDispatchDepartmentsArray(Policy $policy)
    {
        $dispatchDepartmentsArray = [];

        $dispatchDepartments = $policy->getDispatchDepartments();
        foreach ($dispatchDepartments as $dispatchDepartmentsKey) {
            $dispatchDepartmentsArray[] = array(
                    'type' => 'dispatchDepartments',
                    'id' => $dispatchDepartmentsKey->getId()
                );
        }

        return $dispatchDepartmentsArray;
    }

    protected function setUpLabelsArray(Policy $policy)
    {
        $labelsArray = [];

        $labels = $policy->getLabels();
        foreach ($labels as $labelsKey) {
            $labelsArray[] = array(
                    'type' => 'labels',
                    'id' => $labelsKey->getId()
                );
        }

        return $labelsArray;
    }
    
    protected function setUpSpecialsArray(Policy $policy)
    {
        $specialsArray = [];

        $specials = $policy->getSpecials();
        foreach ($specials as $specialsKey) {
            $specialsArray[] = array(
                    'type' => 'policySubjects',
                    'id' => $specialsKey->getId()
                );
        }

        return $specialsArray;
    }
    
    protected function setPolicyProductsArray(Policy $policy)
    {
        $policyProductsArray = [];

        $policyProducts = $policy->getPolicyProducts();
        foreach ($policyProducts as $policyProductsKey) {
            $policyProductsArray[] = $this->getPolicyProductRestfulTranslator()
            ->objectToArray($policyProductsKey);
        }

        return $policyProductsArray;
    }

    protected function setUpApplicableObjectsArray(Policy $policy)
    {
        $applicableObjectsArray = [];

        $applicableObjects = $policy->getApplicableObjects();
        foreach ($applicableObjects as $applicableObjectsKey) {
            $applicableObjectsArray[] = array(
                    'type' => 'dictionaries',
                    'id' => $applicableObjectsKey->getId()
                );
        }

        return $applicableObjectsArray;
    }

    protected function setUpApplicableIndustriesArray(Policy $policy)
    {
        $applicableIndustriesArray = [];

        $applicableIndustries = $policy->getApplicableIndustries();
        foreach ($applicableIndustries as $applicableIndustriesKey) {
            $applicableIndustriesArray[] = array(
                    'type' => 'dictionaries',
                    'id' => $applicableIndustriesKey->getId()
                );
        }

        return $applicableIndustriesArray;
    }

    protected function setUpClassifiesArray(Policy $policy)
    {
        $classifiesArray = [];

        $classifies = $policy->getClassifies();
        foreach ($classifies as $classifiesKey) {
            $classifiesArray[] = array(
                    'type' => 'dictionaries',
                    'id' => $classifiesKey->getId()
                );
        }

        return $classifiesArray;
    }
}
