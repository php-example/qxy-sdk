<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Policy\Model\Policy;
use Sdk\Policy\Model\NullPolicy;
use Sdk\Policy\Model\PolicyModelFactory;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Crew\Translator\CrewRestfulTranslator;
use Sdk\Label\Translator\LabelRestfulTranslator;
use Sdk\PolicySubject\Translator\PolicySubjectRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;
use Sdk\PolicyProduct\Translator\PolicyProductRestfulTranslator;
use Sdk\DispatchDepartment\Translator\DispatchDepartmentRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class PolicyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait, PolicyRestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function getLabelRestfulTranslator()
    {
        return new LabelRestfulTranslator();
    }

    public function getPolicySubjectRestfulTranslator()
    {
        return new PolicySubjectRestfulTranslator();
    }

    public function getDictionaryRestfulTranslator()
    {
        return new DictionaryRestfulTranslator();
    }

    protected function getPolicyProductRestfulTranslator()
    {
        return new PolicyProductRestfulTranslator();
    }

    public function getDispatchDepartmentRestfulTranslator()
    {
        return new DispatchDepartmentRestfulTranslator();
    }

    public function arrayToObject(array $expression, $policy = null)
    {
        return $this->translateToObject($expression, $policy);
    }

    protected function translateToObject(array $expression, $policy = null)
    {
        if (empty($expression)) {
            return NullPolicy::getInstance();
        }
        if ($policy === null) {
            $policy = new Policy();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $policy->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $policy->setTitle($attributes['title']);
        }
        if (isset($attributes['tag'])) {
            $policy->setTag($attributes['tag']);
        }
        if (isset($attributes['number'])) {
            $policy->setNumber($attributes['number']);
        }
        if (isset($attributes['detail'])) {
            $policy->setDetail($attributes['detail']);
        }
        if (isset($attributes['description'])) {
            $policy->setDescription($attributes['description']);
        }
        if (isset($attributes['image'])) {
            $policy->setImage($attributes['image']);
        }
        if (isset($attributes['attachments'])) {
            $policy->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['admissibleAddress'])) {
            $policy->setAdmissibleAddress($attributes['admissibleAddress']);
        }
        if (isset($attributes['processingFlow'])) {
            $policy->setProcessingFlow($attributes['processingFlow']);
        }
        if (isset($attributes['statusTime'])) {
            $policy->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $policy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $policy->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $policy->setStatus($attributes['status']);
        }
        if (isset($attributes['pageViews'])) {
            $policy->setPageViews($attributes['pageViews']);
        }
        if (isset($attributes['shareVolume'])) {
            $policy->setShareVolume($attributes['shareVolume']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['applicableObjects']['data'])) {
            $this->setUpApplicableObjects($relationships['applicableObjects']['data'], $policy);
        }
        if (isset($relationships['applicableIndustries']['data'])) {
            $this->setUpApplicableIndustries($relationships['applicableIndustries']['data'], $policy);
        }
        if (isset($relationships['classifies']['data'])) {
            $this->setUpClassifies($relationships['classifies']['data'], $policy);
        }
        if (isset($relationships['level']['data'])) {
            if (isset($expression['included'])) {
                $level = $this->changeArrayFormat(
                    $relationships['level']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $level = $this->changeArrayFormat($relationships['level']['data']);
            }
            $policy->setLevel($this->getDictionaryRestfulTranslator()->arrayToObject($level));
        }
        if (isset($relationships['dispatchDepartments']['data'])) {
            $this->setUpDispatchDepartments($relationships['dispatchDepartments']['data'], $policy);
        }
        if (isset($relationships['labels']['data'])) {
            $this->setUpLabels($relationships['labels']['data'], $policy);
        }
        if (isset($relationships['specials']['data'])) {
            $this->setUpSpecials($relationships['specials']['data'], $policy);
        }
        if (isset($relationships['policyProducts']['data'])) {
            foreach ($relationships['policyProducts']['data'] as $policyProductArray) {
                if (isset($expression['included'])) {
                    $policyProducts = $this->changeArrayFormat($policyProductArray, $expression['included']);
                }

                if (!isset($expression['included'])) {
                    $policyProducts = $this->changeArrayFormat($policyProductArray);
                }

                $policyProductsObject = $this->getPolicyProductRestfulTranslator()->arrayToObject($policyProducts);

                $policy->addPolicyProduct($policyProductsObject);
            }
        }

        return $policy;
    }

    public function objectToArray($policy, array $keys = array())
    {
        $expression = array();

        if (!$policy instanceof Policy) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'applicableObjects',
                'dispatchDepartments',
                'applicableIndustries',
                'level',
                'classifies',
                'detail',
                'description',
                'image',
                'attachments',
                'labels',
                'specials',
                'processingFlow',
                'admissibleAddress',
                'policyProducts',
                'noticeType',
                'crew',
                'tag'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'policies'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $policy->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $policy->getTitle();
        }
        if (in_array('number', $keys)) {
            $attributes['number'] = $policy->getNumber();
        }
        if (in_array('tag', $keys)) {
            $attributes['tag'] = $policy->getTag();
        }
        if (in_array('detail', $keys)) {
            $attributes['detail'] = $policy->getDetail();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $policy->getDescription();
        }
        if (in_array('image', $keys)) {
            $attributes['image'] = $policy->getImage();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $policy->getAttachments();
        }
        if (in_array('processingFlow', $keys)) {
            $attributes['processingFlow'] = $policy->getProcessingFlow();
        }
        if (in_array('admissibleAddress', $keys)) {
            $attributes['admissibleAddress'] = $policy->getAdmissibleAddress();
        }
        if (in_array('noticeType', $keys)) {
            $noticeTypeString = $policy->getNoticeType();

            $noticeTypes = array();
            if (!empty($noticeTypeString)) {
                $noticeTypes = explode(',', $noticeTypeString);
                foreach ($noticeTypes as $key => $noticeType) {
                    $noticeTypes[$key] = marmot_decode($noticeType);
                }
            }

            $attributes['noticeType'] = implode(',',$noticeTypes);
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $policy->getCrew()->getId()
                )
            );
        }
        if (in_array('level', $keys)) {
            $expression['data']['relationships']['level']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $policy->getLevel()->getId()
                )
            );
        }
        if (in_array('applicableObjects', $keys)) {
            $applicableObjectsArray = $this->setUpApplicableObjectsArray($policy);
            $expression['data']['relationships']['applicableObjects']['data'] = $applicableObjectsArray;
        }
        if (in_array('applicableIndustries', $keys)) {
            $applicableIndustriesArray = $this->setUpApplicableIndustriesArray($policy);
            $expression['data']['relationships']['applicableIndustries']['data'] = $applicableIndustriesArray;
        }
        if (in_array('classifies', $keys)) {
            $classifiesArray = $this->setUpClassifiesArray($policy);
            $expression['data']['relationships']['classifies']['data'] = $classifiesArray;
        }
        if (in_array('dispatchDepartments', $keys)) {
            $dispatchDepartmentsArray = $this->setUpDispatchDepartmentsArray($policy);
            $expression['data']['relationships']['dispatchDepartments']['data'] = $dispatchDepartmentsArray;
        }
        if (in_array('labels', $keys)) {
            $labelsArray = $this->setUpLabelsArray($policy);
            $expression['data']['relationships']['labels']['data'] = $labelsArray;
        }
        if (in_array('policyProducts', $keys)) {
            $policyProductsArray = $this->setPolicyProductsArray($policy);
            $expression['data']['relationships']['policyProducts']['data'] = $policyProductsArray;
        }

        return $expression;
    }
}
