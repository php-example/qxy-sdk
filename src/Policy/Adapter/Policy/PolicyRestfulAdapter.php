<?php
namespace Sdk\Policy\Adapter\Policy;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Policy\Model\Policy;
use Sdk\Policy\Model\NullPolicy;
use Sdk\Policy\Translator\PolicyRestfulTranslator;

use Sdk\Common\Utils\ToolTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PolicyRestfulAdapter extends GuzzleAdapter implements IPolicyAdapter
{
    use ToolTrait,
        CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_POLICY_LIST'=>[
                'fields'=>[
                    'policies' =>
                        'dispatchDepartments,image,number,createTime,updateTime,status,level,specials,tag,pageViews,shareVolume'
                ],
                'include'=> 'crew,dispatchDepartments,labels,policyProducts,policyProducts.enterprise,policyProducts.product,level,applicableObjects,applicableIndustries,classifies,specials' //phpcs:ignore
            ],
            'PORTAL_POLICY_LIST'=>[
                'fields'=>[],
                'include'=> 'crew,dispatchDepartments,labels,policyProducts,policyProducts.enterprise,policyProducts.product,level,applicableObjects,applicableIndustries,classifies,specials' //phpcs:ignore
            ],
            'POLICY_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'dispatchDepartments,labels,policyProducts,policyProducts.enterprise,policyProducts.product,level,applicableObjects,applicableIndustries,classifies,specials' //phpcs:ignore
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PolicyRestfulTranslator();
        $this->resource = 'policies';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPolicy::getInstance());
    }

    public function portalFetchOne($id,$type)
    {
        return $this->portalFetchOneAction($id, $type, NullPolicy::getInstance());
    }

    protected function portalFetchOneAction(int $id, int $type, INull $null)
    {
        $header = $this->getHearderData($type);

        $this->get(
            $this->getResource().'/'.$id,
            array(),
            $header
        );

        return $this->isSuccess() ? $this->translateToObject() : $null;
    }

    protected function addAction(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array(
                'title',
                'applicableObjects',
                'dispatchDepartments',
                'applicableIndustries',
                'level',
                'classifies',
                'detail',
                'description',
                'image',
                'attachments',
                'labels',
                'processingFlow',
                'admissibleAddress',
                'noticeType',
                'crew',
                'tag'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    protected function editAction(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array(
                'applicableObjects',
                'dispatchDepartments',
                'applicableIndustries',
                'level',
                'classifies',
                'description',
                'image',
                'attachments',
                'labels',
                'processingFlow',
                'admissibleAddress',
                'tag'
            )
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    public function relationProduct(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array('policyProducts')
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId().'/relationProduct',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    public function cancelRelationProduct(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array('policyProducts')
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId().'/cancelRelationProduct',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    public function relationSpecial(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array('specials')
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId().'/relationSpecial',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    public function addShareRecord(Policy $policy,int $type) : bool
    {
        $header = $this->getHearderData($type);

        $this->post(
            $this->getResource().'/'.$policy->getId().'/addShareRecord',
            array(),
            $header
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }
}
